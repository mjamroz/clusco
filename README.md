# ClusCo: Clustering and Comparison of Protein Models #
*Michał Jamróz, Andrzej Koliński*
![chee.jpg](https://bitbucket.org/repo/pkpoB5/images/2556994650-chee.jpg)

Full text [BMC Bioinformatics 2013, 14:62 doi:10.1186/1471-2105-14-62](http://www.biomedcentral.com/1471-2105/14/62/abstract])


## Installation ##


```
#!bash

mkdir build
cd build
cmake ../
make

```

binaries creates in src directory


**Requirements**: gcc>=4.2, [tclap library](http://tclap.sourceforge.net/). For GPU code compilation: [CUDA package](https://developer.nvidia.com/cuda-downloads) and CUDA compatible gcc version.


## Help ##

```
#!bash

clusco --help
```

or doc/manual.pdf file

MacOSX users: if you have problems with compilation, check if system contains gcc>=4.2 and/or if Xcode with enabled OpenMP (ENABLE_OPENMP_SUPPORT = YES and LD_OPENMP_FLAGS = -fopenmp).