// public domain 2012, Michal jamroz@chem.uw.edu.pl
// gcc -Wall -o pick -O3 pick.cpp

#include <stdio.h>
#include <stdlib.h>
#include <regex.h>
using namespace std;

int main ( int argc, char *argv[] )
{
	regex_t model;

	regcomp(&model, "^MODEL", REG_EXTENDED);
	
    if ( argc == 1 ) /* argc should be 2 for correct execution */
    {
        /* We print argv[0] assuming it is the program name */
        printf( "Pick models with DELTA and save it into pick_out.pdb\nUsage: %s tra1.pdb tra2.pdb tra3.pdb ... DELTA\n", argv[0] );
    }
    else 
    {
		FILE * tra;
		
		int delta = atoi(argv[argc-1]);
		printf ("Delta: %d\n\n",delta);
		int err,model_iter=0;
		char line[80];
		FILE * out = fopen("pick_out.pdb","w");
		
		for (int i=1;i<argc-1;i++) {
			
			printf("Processing %s ...\n",argv[i]);
			tra = fopen(argv[i],"r");
			while (fgets(line,80,tra)) {
				const char* b = line;
				if ((err = regexec(&model, b, 0, NULL, 0)) == 0) 
					model_iter++;
									
				if (model_iter%delta==0) {
					fprintf(out,"%s",line);
				}

				
	

			}
			  fclose(tra);
		
		}

		

    fclose(out);
    }
	regfree(&model);
  
}
