/* Slightly modified functions of C clustering library:
 * 
 * The C clustering library.
 * Copyright (C) 2002 Michiel Jan Laurens de Hoon.
 *
 * This library was written at the Laboratory of DNA Information Analysis,
 * Human Genome Center, Institute of Medical Science, University of Tokyo,
 * 4-6-1 Shirokanedai, Minato-ku, Tokyo 108-8639, Japan.
 * Contact: mdehoon 'AT' gsc.riken.jp
 * 
 * Permission to use, copy, modify, and distribute this software and its
 * documentation with or without modifications and for any purpose and
 * without fee is hereby granted, provided that any copyright notices
 * appear in all copies and that both those copyright notices and this
 * permission notice appear in supporting documentation, and that the
 * names of the contributors or copyright holders not be used in
 * advertising or publicity pertaining to distribution of the software
 * without specific prior permission.
 * 
 * THE CONTRIBUTORS AND COPYRIGHT HOLDERS OF THIS SOFTWARE DISCLAIM ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL THE
 * CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY SPECIAL, INDIRECT
 * OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE
 * OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE
 * OR PERFORMANCE OF THIS SOFTWARE.
 * 
 */

#include <time.h>
#include <stdlib.h>
#include <math.h>
#include <float.h>
#include <limits.h>
#include <string.h>
#ifndef NOOMP
#include <omp.h>
#endif

#include "cluster_light.h"



static
int nodecompare(const void *a, const void *b)
{
    const Node *node1 = (const Node *) a;
    const Node *node2 = (const Node *) b;
    const float term1 = node1->distance;
    const float term2 = node2->distance;
    if (term1 < term2)
	return -1;
    if (term1 > term2)
	return +1;
    return 0;
}

inline float euclid(int n, float **data1, float **data2, int index1,
		    int index2)
{
    float result = 0.;
    int i;
    for (i = 0; i < n; i++) {
	float term = data1[index1][i] - data2[index2][i];
	result += term * term;
    }

    return result;
}

float **distancematrix(int nrows, float **data)
{				/* First determine the size of the distance matrix */
    const int n = nrows;
    const int ndata = nrows;

    float **matrix;
    int i, j;

    /* Set up the ragged array */
    matrix = malloc(n * sizeof(float *));
    if (matrix == NULL)
	return NULL;		/* Not enough memory available */
    matrix[0] = NULL;
    /* The zeroth row has zero columns. We allocate it anyway for convenience. */
    for (i = 1; i < n; i++) {
	matrix[i] = malloc(i * sizeof(float));
	if (matrix[i] == NULL)
	    break;		/* Not enough memory available */
    }
    if (i < n) {		/* break condition encountered */
	int j = i;
	for (i = 1; i < j; i++)
	    free(matrix[i]);
	return NULL;
    }
    /* Calculate the distances and save them in the ragged array */
#pragma omp parallel for  schedule(dynamic) private(i,j)
    for (i = 1; i < n; i++) {
	for (j = 0; j < i; j++) {
	    matrix[i][j] = euclid(ndata, data, data, i, j);
	    //    printf("%d %d %7.2f\n",i,j,matrix[i][j]);
	}
    }

    return matrix;
}

static
float find_closest_pair(int n, float **distmatrix, int *ip, int *jp)
{
    int i, j;
    float temp;
    float distance = distmatrix[1][0];
    *ip = 1;
    *jp = 0;
    for (i = 1; i < n; i++) {
	for (j = 0; j < i; j++) {
	    temp = distmatrix[i][j];
	    if (temp < distance) {
		distance = temp;
		*ip = i;
		*jp = j;
	    }
	}
    }
    return distance;
}

static Node *palcluster(int nelements, float **distmatrix)
{
    int j;
    int n;
    int *clusterid;
    int *number;
    Node *result;

    clusterid = malloc(nelements * sizeof(int));
    if (!clusterid)
	return NULL;
    number = malloc(nelements * sizeof(int));
    if (!number) {
	free(clusterid);
	return NULL;
    }
    result = malloc((nelements - 1) * sizeof(Node));
    if (!result) {
	free(clusterid);
	free(number);
	return NULL;
    }

    /* Setup a list specifying to which cluster a gene belongs, and keep track
     * of the number of elements in each cluster (needed to calculate the
     * average). */
    for (j = 0; j < nelements; j++) {
	number[j] = 1;
	clusterid[j] = j;
    }

    for (n = nelements; n > 1; n--) {
	int sum;
	int is = 1;
	int js = 0;
	result[nelements - n].distance =
	    find_closest_pair(n, distmatrix, &is, &js);

	/* Save result */
	result[nelements - n].left = clusterid[is];
	result[nelements - n].right = clusterid[js];

	/* Fix the distances */
	sum = number[is] + number[js];
	for (j = 0; j < js; j++) {
	    distmatrix[js][j] = distmatrix[is][j] * number[is]
		+ distmatrix[js][j] * number[js];
	    distmatrix[js][j] /= sum;
	}
	for (j = js + 1; j < is; j++) {
	    distmatrix[j][js] = distmatrix[is][j] * number[is]
		+ distmatrix[j][js] * number[js];
	    distmatrix[j][js] /= sum;
	}
	for (j = is + 1; j < n; j++) {
	    distmatrix[j][js] = distmatrix[j][is] * number[is]
		+ distmatrix[j][js] * number[js];
	    distmatrix[j][js] /= sum;
	}

	for (j = 0; j < is; j++)
	    distmatrix[is][j] = distmatrix[n - 1][j];
	for (j = is + 1; j < n - 1; j++)
	    distmatrix[j][is] = distmatrix[n - 1][j];

	/* Update number of elements in the clusters */
	number[js] = sum;
	number[is] = number[n - 1];

	/* Update clusterids */
	clusterid[js] = n - nelements - 1;
	clusterid[is] = clusterid[n - 1];
    }
    free(clusterid);
    free(number);

    return result;
}

static Node *pmlcluster(int nelements, float **distmatrix)
{
    int j;
    int n;
    int *clusterid;
    Node *result;

    clusterid = malloc(nelements * sizeof(int));
    if (!clusterid)
	return NULL;
    result = malloc((nelements - 1) * sizeof(Node));
    if (!result) {
	free(clusterid);
	return NULL;
    }

    /* Setup a list specifying to which cluster a gene belongs */
    for (j = 0; j < nelements; j++)
	clusterid[j] = j;

    for (n = nelements; n > 1; n--) {
	int is = 1;
	int js = 0;
	result[nelements - n].distance =
	    find_closest_pair(n, distmatrix, &is, &js);

	/* Fix the distances */
	for (j = 0; j < js; j++)
	    distmatrix[js][j] = max(distmatrix[is][j], distmatrix[js][j]);
	for (j = js + 1; j < is; j++)
	    distmatrix[j][js] = max(distmatrix[is][j], distmatrix[j][js]);
	for (j = is + 1; j < n; j++)
	    distmatrix[j][js] = max(distmatrix[j][is], distmatrix[j][js]);

	for (j = 0; j < is; j++)
	    distmatrix[is][j] = distmatrix[n - 1][j];
	for (j = is + 1; j < n - 1; j++)
	    distmatrix[j][is] = distmatrix[n - 1][j];

	/* Update clusterids */
	result[nelements - n].left = clusterid[is];
	result[nelements - n].right = clusterid[js];
	clusterid[js] = n - nelements - 1;
	clusterid[is] = clusterid[n - 1];
    }
    free(clusterid);

    return result;
}

static
Node *pslcluster(int nelements, float **data, float **distmatrix)
{
    int i, j, k;
    const int nnodes = nelements - 1;
    int *vector;
    float *temp;
    int *index;
    Node *result;
    temp = malloc(nnodes * sizeof(float));
    if (!temp)
	return NULL;
    index = malloc(nelements * sizeof(int));
    if (!index) {
	free(temp);
	return NULL;
    }
    vector = malloc(nnodes * sizeof(int));
    if (!vector) {
	free(index);
	free(temp);
	return NULL;
    }
    result = malloc(nelements * sizeof(Node));
    if (!result) {
	free(vector);
	free(index);
	free(temp);
	return NULL;
    }

    for (i = 0; i < nnodes; i++)
	vector[i] = i;

    if (distmatrix) {
	for (i = 0; i < nelements; i++) {
	    result[i].distance = FLT_MAX;
	    for (j = 0; j < i; j++)
		temp[j] = distmatrix[i][j];
	    for (j = 0; j < i; j++) {
		k = vector[j];
		if (result[j].distance >= temp[j]) {
		    if (result[j].distance < temp[k])
			temp[k] = result[j].distance;
		    result[j].distance = temp[j];
		    vector[j] = i;
		} else if (temp[j] < temp[k])
		    temp[k] = temp[j];
	    }
	    for (j = 0; j < i; j++) {
		if (result[j].distance >= result[vector[j]].distance)
		    vector[j] = i;
	    }
	}
    } else {
	const int ndata = nelements;

	for (i = 0; i < nelements; i++) {
	    result[i].distance = FLT_MAX;
	    for (j = 0; j < i; j++)
		temp[j] = euclid(ndata, data, data, i, j);
	    for (j = 0; j < i; j++) {
		k = vector[j];
		if (result[j].distance >= temp[j]) {
		    if (result[j].distance < temp[k])
			temp[k] = result[j].distance;
		    result[j].distance = temp[j];
		    vector[j] = i;
		} else if (temp[j] < temp[k])
		    temp[k] = temp[j];
	    }
	    for (j = 0; j < i; j++)
		if (result[j].distance >= result[vector[j]].distance)
		    vector[j] = i;
	}
    }
    free(temp);

    for (i = 0; i < nnodes; i++)
	result[i].left = i;
    qsort(result, nnodes, sizeof(Node), nodecompare);

    for (i = 0; i < nelements; i++)
	index[i] = i;
    for (i = 0; i < nnodes; i++) {
	j = result[i].left;
	k = vector[j];
	result[i].left = index[j];
	result[i].right = index[k];
	index[k] = -i - 1;
    }
    free(vector);
    free(index);

    result = realloc(result, nnodes * sizeof(Node));

    return result;
}



Node *treecluster(int nrows, float **data, char method, float **distmatrix)
{
    Node *result = NULL;
    const int nelements = nrows;
    const int ldistmatrix = (distmatrix == NULL && method != 's') ? 1 : 0;

    if (nelements < 2)
	return NULL;

    /* Calculate the distance matrix if the user didn't give it */
    if (ldistmatrix) {
	distmatrix = distancematrix(nrows, data);
	if (!distmatrix)
	    return NULL;	/* Insufficient memory */
    }

    switch (method) {
    case 's':
	result = pslcluster(nelements, data, distmatrix);
	break;
    case 'm':
	result = pmlcluster(nelements, distmatrix);
	break;
    case 'a':
	result = palcluster(nelements, distmatrix);
	break;
    }

    /* Deallocate space for distance matrix, if it was allocated by treecluster */
    if (ldistmatrix) {
	int i;
	for (i = 1; i < nelements; i++)
	    free(distmatrix[i]);
	free(distmatrix);
    }

    return result;
}


void cuttree(int nelements, Node * tree, int nclusters, int clusterid[])
{
    int i, j, k;
    int icluster = 0;
    const int n = nelements - nclusters;	/* number of nodes to join */
    int *nodeid;
    for (i = nelements - 2; i >= n; i--) {
	k = tree[i].left;
	if (k >= 0) {
	    clusterid[k] = icluster;
	    icluster++;
	}
	k = tree[i].right;
	if (k >= 0) {
	    clusterid[k] = icluster;
	    icluster++;
	}
    }
    nodeid = malloc(n * sizeof(int));
    if (!nodeid) {
	for (i = 0; i < nelements; i++)
	    clusterid[i] = -1;
	return;
    }
    for (i = 0; i < n; i++)
	nodeid[i] = -1;
    for (i = n - 1; i >= 0; i--) {
	if (nodeid[i] < 0) {
	    j = icluster;
	    nodeid[i] = j;
	    icluster++;
	} else
	    j = nodeid[i];
	k = tree[i].left;
	if (k < 0)
	    nodeid[-k - 1] = j;
	else
	    clusterid[k] = j;
	k = tree[i].right;
	if (k < 0)
	    nodeid[-k - 1] = j;
	else
	    clusterid[k] = j;
    }
    free(nodeid);
    return;
}


static float uniform(void)
/*
Purpose
=======

This routine returns a uniform random number between 0.0 and 1.0. Both 0.0
and 1.0 are excluded. This random number generator is described in:

Pierre l'Ecuyer
Efficient and Portable Combined Random Number Generators
Communications of the ACM, Volume 31, Number 6, June 1988, pages 742-749,774.

The first time this routine is called, it initializes the random number
generator using the current time. First, the current epoch time in seconds is
used as a seed for the random number generator in the C library. The first two
random numbers generated by this generator are used to initialize the random
number generator implemented in this routine.


Arguments
=========

None.


Return value
============

A double-precison number between 0.0 and 1.0.
============================================================================
*/
{
    int z;
    static const int m1 = 2147483563;
    static const int m2 = 2147483399;
    const double scale = 1.0 / m1;

    static int s1 = 0;
    static int s2 = 0;

    if (s1 == 0 || s2 == 0) {	/* initialize */
	unsigned int initseed = (unsigned int) time(0);
	srand(initseed);
	s1 = rand();
	s2 = rand();
    }

    do {
	int k;
	k = s1 / 53668;
	s1 = 40014 * (s1 - k * 53668) - k * 12211;
	if (s1 < 0)
	    s1 += m1;
	k = s2 / 52774;
	s2 = 40692 * (s2 - k * 52774) - k * 3791;
	if (s2 < 0)
	    s2 += m2;
	z = s1 - s2;
	if (z < 1)
	    z += (m1 - 1);
    } while (z == m1);		/* To avoid returning 1.0 */

    return (float) z *scale;
}

/* ************************************************************************ */

static int binomial(int n, double p)
/*
Purpose
=======

This routine generates a random number between 0 and n inclusive, following
the binomial distribution with probability p and n trials. The routine is
based on the BTPE algorithm, described in:

Voratas Kachitvichyanukul and Bruce W. Schmeiser:
Binomial Random Variate Generation
Communications of the ACM, Volume 31, Number 2, February 1988, pages 216-222.


Arguments
=========

p          (input) double
The probability of a single event. This probability should be less than or
equal to 0.5.

n          (input) int
The number of trials.


Return value
============

An integer drawn from a binomial distribution with parameters (p, n).

============================================================================
*/
{
    const double q = 1 - p;
    if (n * p < 30.0) {		/* Algorithm BINV */
	const double s = p / q;
	const double a = (n + 1) * s;
	double r = exp(n * log(q));	/* pow() causes a crash on AIX */
	int x = 0;
	double u = uniform();
	while (1) {
	    if (u < r)
		return x;
	    u -= r;
	    x++;
	    r *= (a / x) - s;
	}
    } else {			/* Algorithm BTPE */
	/* Step 0 */
	const double fm = n * p + p;
	const int m = (int) fm;
	const double p1 = floor(2.195 * sqrt(n * p * q) - 4.6 * q) + 0.5;
	const double xm = m + 0.5;
	const double xl = xm - p1;
	const double xr = xm + p1;
	const double c = 0.134 + 20.5 / (15.3 + m);
	const double a = (fm - xl) / (fm - xl * p);
	const double b = (xr - fm) / (xr * q);
	const double lambdal = a * (1.0 + 0.5 * a);
	const double lambdar = b * (1.0 + 0.5 * b);
	const double p2 = p1 * (1 + 2 * c);
	const double p3 = p2 + c / lambdal;
	const double p4 = p3 + c / lambdar;
	while (1) {		/* Step 1 */
	    int y;
	    int k;
	    double u = uniform();
	    double v = uniform();
	    u *= p4;
	    if (u <= p1)
		return (int) (xm - p1 * v + u);
	    /* Step 2 */
	    if (u > p2) {	/* Step 3 */
		if (u > p3) {	/* Step 4 */
		    y = (int) (xr - log(v) / lambdar);
		    if (y > n)
			continue;
		    /* Go to step 5 */
		    v = v * (u - p3) * lambdar;
		} else {
		    y = (int) (xl + log(v) / lambdal);
		    if (y < 0)
			continue;
		    /* Go to step 5 */
		    v = v * (u - p2) * lambdal;
		}
	    } else {
		const double x = xl + (u - p1) / c;
		v = v * c + 1.0 - fabs(m - x + 0.5) / p1;
		if (v > 1)
		    continue;
		/* Go to step 5 */
		y = (int) x;
	    }
	    /* Step 5 */
	    /* Step 5.0 */
	    k = abs(y - m);
	    if (k > 20 && k < 0.5 * n * p * q - 1.0) {	/* Step 5.2 */
		double rho =
		    (k / (n * p * q)) *
		    ((k * (k / 3.0 + 0.625) +
		      0.1666666666666) / (n * p * q) + 0.5);
		double t = -k * k / (2 * n * p * q);
		double A = log(v);
		if (A < t - rho)
		    return y;
		else if (A > t + rho)
		    continue;
		else {		/* Step 5.3 */
		    double x1 = y + 1;
		    double f1 = m + 1;
		    double z = n + 1 - m;
		    double w = n - y + 1;
		    double x2 = x1 * x1;
		    double f2 = f1 * f1;
		    double z2 = z * z;
		    double w2 = w * w;
		    if (A > xm * log(f1 / x1) + (n - m + 0.5) * log(z / w)
			+ (y - m) * log(w * p / (x1 * q))
			+ (13860. -
			   (462. -
			    (132. -
			     (99. -
			      140. / f2) / f2) / f2) / f2) / f1 / 166320. +
			(13860. -
			 (462. -
			  (132. -
			   (99. -
			    140. / z2) / z2) / z2) / z2) / z / 166320. +
			(13860. -
			 (462. -
			  (132. -
			   (99. -
			    140. / x2) / x2) / x2) / x2) / x1 / 166320. +
			(13860. -
			 (462. -
			  (132. -
			   (99. -
			    140. / w2) / w2) / w2) / w2) / w / 166320.)
			continue;
		    return y;
		}
	    } else {		/* Step 5.1 */
		int i;
		const double s = p / q;
		const double aa = s * (n + 1);
		double f = 1.0;
		for (i = m; i < y; f *= (aa / (++i) - s));
		for (i = y; i < m; f /= (aa / (++i) - s));
		if (v > f)
		    continue;
		return (float) y;
	    }
	}
    }
    /* Never get here */
    return -1;
}

/* ************************************************************************ */


static void randomassign(int nclusters, int nelements, int clusterid[])
/*
Purpose
=======

The randomassign routine performs an initial random clustering, needed for
k-means or k-median clustering. Elements (genes or microarrays) are randomly
assigned to clusters. The number of elements in each cluster is chosen
randomly, making sure that each cluster will receive at least one element.


Arguments
=========

nclusters  (input) int
The number of clusters.

nelements  (input) int
The number of elements to be clustered (i.e., the number of genes or microarrays
to be clustered).

clusterid  (output) int[nelements]
The cluster number to which an element was assigned.

============================================================================
*/
{
    int i, j;
    int k = 0;
    float p;
    int n = nelements - nclusters;
    /* Draw the number of elements in each cluster from a multinomial
     * distribution, reserving ncluster elements to set independently
     * in order to guarantee that none of the clusters are empty.
     */
    for (i = 0; i < nclusters - 1; i++) {
	p = 1.0 / (nclusters - i);
	j = binomial(n, p);
	n -= j;
	j += k + 1;		/* Assign at least one element to cluster i */
	for (; k < j; k++)
	    clusterid[k] = i;
    }
    /* Assign the remaining elements to the last cluster */
    for (; k < nelements; k++)
	clusterid[k] = i;

    /* Create a random permutation of the cluster assignments */
    for (i = 0; i < nelements; i++) {
	j = (int) (i + (nelements - i) * uniform());
	k = clusterid[j];
	clusterid[j] = clusterid[i];
	clusterid[i] = k;
    }
//for(i=0;i<nclusters;i++)
//      printf("%d\n",clusterid[i]);
    return;
}

void getclustermedoids(int nclusters, int nelements, float **distance,
		       int clusterid[], int centroids[], float errors[])
{
    int i, j, k;
    for (j = 0; j < nclusters; j++)
	errors[j] = FLT_MAX;
    for (i = 0; i < nelements; i++) {
	float d = 0.0;
	j = clusterid[i];
	for (k = 0; k < nelements; k++) {
	    if (i == k || clusterid[k] != j)
		continue;
	    d += (i < k ? distance[k][i] : distance[i][k]);
	    if (d > errors[j])
		break;
	}
	if (d < errors[j]) {
	    errors[j] = d;
	    centroids[j] = i;
	}
    }
}

/* ********************************************************************* */


void kmedoids(int nclusters, int nelements, float **distmatrix,
	      int npass, int clusterid[], float *error, int *ifound)
{
    int i, j, icluster;
    int *tclusterid;
    int *saved;
    int *centroids;
    float *errors;
    int ipass = 0;

    if (nelements < nclusters) {
	*ifound = 0;
	return;
    }
    /* More clusters asked for than elements available */
    *ifound = -1;

    /* We save the clustering solution periodically and check if it reappears */
    saved = malloc(nelements * sizeof(int));
    if (saved == NULL)
	return;

    centroids = malloc(nclusters * sizeof(int));
    if (!centroids) {
	free(saved);
	return;
    }

    errors = malloc(nclusters * sizeof(float));
    if (!errors) {
	free(saved);
	free(centroids);
	return;
    }

    /* Find out if the user specified an initial clustering */
    if (npass <= 1)
	tclusterid = clusterid;
    else {
	tclusterid = malloc(nelements * sizeof(int));
	if (!tclusterid) {
	    free(saved);
	    free(centroids);
	    free(errors);
	    return;
	}
    }

    *error = FLT_MAX;
    do {			/* Start the loop */
	float total = FLT_MAX;
	int counter = 0;
	int period = 10;

	if (npass != 0)
	    randomassign(nclusters, nelements, tclusterid);


	while (1) {
	    float previous = total;
	    total = 0.0;

	    if (counter % period == 0) {	/* Save the current cluster assignments */
		for (i = 0; i < nelements; i++)
		    saved[i] = tclusterid[i];
		if (period < INT_MAX / 2)
		    period *= 2;
	    }
	    counter++;

	    /* Find the center */
	    getclustermedoids(nclusters, nelements, distmatrix, tclusterid,
			      centroids, errors);

	    for (i = 0; i < nelements; i++) {
		float distance = FLT_MAX;
		for (icluster = 0; icluster < nclusters; icluster++) {
		    float tdistance;
		    j = centroids[icluster];
		    if (i == j) {
			distance = 0.0;
			tclusterid[i] = icluster;
			break;
		    }
		    tdistance =
			(i > j) ? distmatrix[i][j] : distmatrix[j][i];
		    if (tdistance < distance) {
			distance = tdistance;
			tclusterid[i] = icluster;
		    }
		}
		total += distance;
	    }
	    if (total >= previous)
		break;
	    /* total>=previous is FALSE on some machines even if total and previous
	     * are bitwise identical. */
	    for (i = 0; i < nelements; i++)
		if (saved[i] != tclusterid[i])
		    break;
	    if (i == nelements)
		break;		/* Identical solution found; break out of this loop */
	}

	for (i = 0; i < nelements; i++) {
	    if (clusterid[i] != centroids[tclusterid[i]]) {
		if (total < *error) {
		    *ifound = 1;
		    *error = total;
		    /* Replace by the centroid in each cluster. */
		    for (j = 0; j < nelements; j++)
			clusterid[j] = centroids[tclusterid[j]];
		}
		break;
	    }
	}
	if (i == nelements)
	    (*ifound)++;	/* break statement not encountered */
    } while (++ipass < npass);

    /* Deallocate temporarily used space */
    if (npass > 1)
	free(tclusterid);

    free(saved);


    free(centroids);
    free(errors);
    return;
}
