// omp_kmeans, euclid_dist_2 and find_nearest_cluster license:
// Copyright (c) 2005 Wei-keng Liao
// Copyright (c) 2011 Serban Giuroiu
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

// -----------------------------------------------------------------------------



#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#ifndef NOOMP
#include <omp.h>
#endif
#include "kmeans.h"
#include "clustering.h"
#include "cluster_light.h"
using namespace std;
float **seq_kmeans(float **objects,	/* in: [numObjs][numCoords] */
		   int numCoords,	/* no. features */
		   int numObjs,	/* no. objects */
		   int numClusters,	/* no. clusters */
		   float threshold,	/* % objects change membership */
		   int *membership);
int *selectClusterCenter(float **objects, int numCoords, int numClusters);
int *kMeansClustering2(float **data, int conformers, float threshold,
		       int numClusters)
{

    int *membership = new int[conformers];
    omp_kmeans(false, data, conformers, conformers, numClusters, threshold,
	       membership);

    return membership;
}



/*----< euclid_dist_2() >----------------------------------------------------*/
/* square of Euclid distance between two multi-dimensional points            */
__inline static
float euclid_dist_2(int numdims,	/* no. dimensions */
		    float *coord1,	/* [numdims] */
		    float *coord2)
{				/* [numdims] */
    int i;
    float ans = 0.0;

    for (i = 0; i < numdims; i++)
	ans += (coord1[i] - coord2[i]) * (coord1[i] - coord2[i]);

    return (ans);
}

/*----< find_nearest_cluster() >---------------------------------------------*/
__inline static
int find_nearest_cluster(int numClusters,	/* no. clusters */
			 int numCoords,	/* no. coordinates */
			 float *object,	/* [numCoords] */
			 float **clusters)
{				/* [numClusters][numCoords] */
    int index, i;
    float dist, min_dist;

    /* find the cluster id that has min distance to object */
    index = 0;
    min_dist = euclid_dist_2(numCoords, object, clusters[0]);

    for (i = 1; i < numClusters; i++) {
	dist = euclid_dist_2(numCoords, object, clusters[i]);
	/* no need square root */
	if (dist < min_dist) {	/* find the min and its array index */
	    min_dist = dist;
	    index = i;
	}
    }
    return (index);
}

int *selectClusterCenter(float **data, int conformers, int k)
{
    // according to Hartigan & Wong, "a k-means clustering algorithm". Added by MJamroz

    // calculate mean:
    float *mean = new float[conformers];
    int *indexes = new int[conformers];

    for (int i = 0; i < conformers; i++) {
	mean[i] = 0.0;
	indexes[i] = i;
    }

    for (int i = 0; i < conformers; i++)
	for (int j = 0; j < conformers; j++)
	    mean[j] += data[i][j];

    for (int j = 0; j < conformers; j++)
	mean[j] /= conformers;
    // calc distance to the mean 
    float *distance = new float[conformers];
    for (int i = 0; i < conformers; i++)
	distance[i] = euclid_dist_2(conformers, mean, data[i]);

    delete[]mean;

    //sort by distance. first is largest distance. sorry for bubble sort
    int tmpi = -1;
    float tmpv = 0;
    for (int i = 0; i < conformers; i++) {
	for (int j = 0; j < conformers; j++) {
	    if (distance[j] < distance[i]) {
		tmpi = indexes[j];
		tmpv = distance[j];
		distance[j] = distance[i];
		indexes[j] = indexes[i];
		distance[i] = tmpv;
		indexes[i] = tmpi;
	    }
	}
    }


    // unique list
    vector < int >uindexes;
    int prev = 0;
    uindexes.push_back(indexes[0]);
    for (int i = 1; i < conformers; i++) {
	if (distance[prev] == distance[i])
	    continue;
	uindexes.push_back(indexes[i]);
	prev = i;

    }
    delete[]distance;

    int *seed = new int[k];
    int s = uindexes.size();
    if (s <= k)			// wybierz w petli z nich k indeksow
    {
	int kk = k - 1;
	int i = 0;

	while (kk >= 0) {
	    seed[kk--] = uindexes[i++];
	    if (i == s)
		i = 0;		//rewind
	}

    } else {

	for (int i = 1; i <= k; i++) {
	    int v = (1 + (i - 1) * (int) (s / k));
	    seed[i - 1] = uindexes[v];
	    //  printf("K %d %6.2f %d\n",v,distance[v], indexes[v]);
	}
    }
//    for (int g=0;g<k;g++)
    //    printf("%d %d\n",g,seed[g]); 
    delete[]indexes;
    return seed;

}

/*----< kmeans_clustering() >------------------------------------------------*/
/* return an array of cluster centers of size [numClusters][numCoords]       */
float **omp_kmeans(int is_perform_atomic,	/* in: */
		   float **objects,	/* in: [numObjs][numCoords] */
		   int numCoords,	/* no. coordinates */
		   int numObjs,	/* no. objects */
		   int numClusters,	/* no. clusters */
		   float threshold,	/* % objects change membership */
		   int *membership)
{				/* out: [numObjs] */

    int i, j, k, index, loop = 0;
    int *newClusterSize;	/* [numClusters]: no. objects assigned in each
				   new cluster */
    float delta;		/* % of objects change their clusters */
    float **clusters;		/* out: [numClusters][numCoords] */
    float **newClusters;	/* [numClusters][numCoords] */

    int nthreads;		/* no. threads */
    int **local_newClusterSize;	/* [nthreads][numClusters] */
    float ***local_newClusters;	/* [nthreads][numClusters][numCoords] */

#ifndef NOOMP
    nthreads = omp_get_max_threads();
#else
    nthreads = 1;
#endif
    /* allocate a 2D space for returning variable clusters[] (coordinates
       of cluster centers) */
    clusters = (float **) malloc(numClusters * sizeof(float *));
    assert(clusters != NULL);
    clusters[0] =
	(float *) malloc(numClusters * numCoords * sizeof(float));
    assert(clusters[0] != NULL);
    for (i = 1; i < numClusters; i++) {
	clusters[i] = clusters[i - 1] + numCoords;
//      printf("%f %d %d\n",clusters[i][1],i,numCoords);
    }

    /* pick first numClusters elements of objects[] as initial cluster centers */
//    int * seed = selectClusterCenter(objects, numCoords, numClusters);
    for (i = 0; i < numClusters; i++)
	for (j = 0; j < numCoords; j++)
	    clusters[i][j] = objects[i][j];

    /* initialize membership[] */
    for (i = 0; i < numObjs; i++)
	membership[i] = -1;

    /* need to initialize newClusterSize and newClusters[0] to all 0 */
    newClusterSize = (int *) calloc(numClusters, sizeof(int));
    assert(newClusterSize != NULL);

    newClusters = (float **) malloc(numClusters * sizeof(float *));
    assert(newClusters != NULL);
    newClusters[0] =
	(float *) calloc(numClusters * numCoords, sizeof(float));
    assert(newClusters[0] != NULL);
    for (i = 1; i < numClusters; i++)
	newClusters[i] = newClusters[i - 1] + numCoords;


    /* each thread calculates new centers using a private space,
       then thread 0 does an array reduction on them. This approach
       should be faster */
    local_newClusterSize = (int **) malloc(nthreads * sizeof(int *));
    assert(local_newClusterSize != NULL);
    local_newClusterSize[0] = (int *) calloc(nthreads * numClusters,
					     sizeof(int));
    assert(local_newClusterSize[0] != NULL);
    for (i = 1; i < nthreads; i++)
	local_newClusterSize[i] =
	    local_newClusterSize[i - 1] + numClusters;

    /* local_newClusters is a 3D array */
    local_newClusters = (float ***) malloc(nthreads * sizeof(float **));
    assert(local_newClusters != NULL);
    local_newClusters[0] = (float **) malloc(nthreads * numClusters *
					     sizeof(float *));
    assert(local_newClusters[0] != NULL);
    for (i = 1; i < nthreads; i++)
	local_newClusters[i] = local_newClusters[i - 1] + numClusters;
    for (i = 0; i < nthreads; i++) {
	for (j = 0; j < numClusters; j++) {
	    local_newClusters[i][j] = (float *) calloc(numCoords,
						       sizeof(float));
	    assert(local_newClusters[i][j] != NULL);
	}
    }


    do {
	delta = 0.0;

	if (is_perform_atomic) {
#pragma omp parallel for \
                    private(i,j,index) \
                    firstprivate(numObjs,numClusters,numCoords) \
                    shared(objects,clusters,membership,newClusters,newClusterSize) \
                    schedule(static) \
                    reduction(+:delta)
	    for (i = 0; i < numObjs; i++) {

		index =
		    find_nearest_cluster(numClusters, numCoords,
					 objects[i], clusters);


		if (membership[i] != index)
		    delta += 1.0;


		membership[i] = index;


#pragma omp atomic
		newClusterSize[index]++;
		for (j = 0; j < numCoords; j++)
#pragma omp atomic
		    newClusters[index][j] += objects[i][j];
	    }
	} else {
#pragma omp parallel \
                    shared(objects,clusters,membership,local_newClusters,local_newClusterSize)
	    {
#ifndef NOOMP
		int tid = omp_get_thread_num();
#else
                int tid = 0;
#endif

#pragma omp for \
                            private(i,j,index) \
                            firstprivate(numObjs,numClusters,numCoords) \
                            schedule(static) \
                            reduction(+:delta)
		for (i = 0; i < numObjs; i++) {
		    /* find the array index of nestest cluster center */
		    index = find_nearest_cluster(numClusters, numCoords,
						 objects[i], clusters);

		    /* if membership changes, increase delta by 1 */
		    if (membership[i] != index)
			delta += 1.0;

		    /* assign the membership to object i */
		    membership[i] = index;

		    /* update new cluster centers : sum of all objects located
		       within (average will be performed later) */
		    local_newClusterSize[tid][index]++;
		    for (j = 0; j < numCoords; j++)
			local_newClusters[tid][index][j] += objects[i][j];
		}
	    }			/* end of #pragma omp parallel */

	    /* let the main thread perform the array reduction */
	    for (i = 0; i < numClusters; i++) {
		for (j = 0; j < nthreads; j++) {
		    newClusterSize[i] += local_newClusterSize[j][i];
		    local_newClusterSize[j][i] = 0.0;
		    for (k = 0; k < numCoords; k++) {
			newClusters[i][k] += local_newClusters[j][i][k];
			local_newClusters[j][i][k] = 0.0;
		    }
		}
	    }
	}

	/* average the sum and replace old cluster centers with newClusters */
	for (i = 0; i < numClusters; i++) {
	    for (j = 0; j < numCoords; j++) {
		if (newClusterSize[i] > 1)
		    clusters[i][j] = newClusters[i][j] / newClusterSize[i];
		newClusters[i][j] = 0.0;	/* set back to 0 */
	    }
	    newClusterSize[i] = 0;	/* set back to 0 */
	}

	delta /= numObjs;
    } while (delta > threshold && loop++ < 5000);



    if (!is_perform_atomic) {
	free(local_newClusterSize[0]);
	free(local_newClusterSize);

	for (i = 0; i < nthreads; i++)
	    for (j = 0; j < numClusters; j++)
		free(local_newClusters[i][j]);
	free(local_newClusters[0]);
	free(local_newClusters);
    }
    free(newClusters[0]);
    free(newClusters);
    free(newClusterSize);

    return clusters;
}
