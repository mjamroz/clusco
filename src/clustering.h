int *kMeansClustering2(float **data, int conformers, float threshold,
		       int numClusters);

int *selectClusterCenter(float **data, int conformers, int k);
