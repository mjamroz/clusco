#include <math.h>
#ifndef NOOMP
#include <omp.h>
#endif
#include <iostream>
#include <stdio.h>

using namespace std;


inline float dist(float *coordinates, int conformer, int pl, int r2,
		  int r1)
{

    int si = 3 * conformer * pl;
    int r1i = si + 3 * r1;
    int r2i = si + 3 * r2;
    float x = coordinates[r1i] - coordinates[r2i];
    float y = coordinates[r1i + 1] - coordinates[r2i + 1];
    float z = coordinates[r1i + 2] - coordinates[r2i + 2];

    return sqrt(x * x + y * y + z * z);
}


unsigned short *CMOOneRef(float *reference, float *data, long conformers,
			  int protlen, float cutoff)
{

    int i;
    bool *refcontacts = new bool[protlen * (protlen - 1) / 2];
    int refcontacts_len = 0;
    int *contacts_positive = new int[conformers];
    bool **contacts = new bool *[conformers];
    for (i = 0; i < conformers; i++)
	contacts[i] = new bool[(protlen * (protlen - 1) / 2)];
    // reference contact
    for (int j = 0; j < protlen - 1; j++) {
	int iter = 0;
	for (int j2 = j + 1; j2 < protlen; j2++) {
	    int index = iter + j * (protlen - 1) - j * (j - 1) / 2;
	    iter++;
	    if (dist(reference, 0, protlen, j, j2) <= cutoff) {
		refcontacts[index] = true;
		refcontacts_len++;
	    } else
		refcontacts[index] = false;
	}
    }

    // list contact    
    for (i = 0; i < conformers; i++) {
	for (int j = 0; j < protlen - 1; j++) {
	    int iter = 0;
	    for (int j2 = j + 1; j2 < protlen; j2++) {
		int index = iter + j * (protlen - 1) - j * (j - 1) / 2;
		iter++;
		//    printf("%d %d\n",index,protlen*(protlen-1)/2);
		if (dist(data, i, protlen, j, j2) <= cutoff) {
		    contacts[i][index] = true;
		    contacts_positive[i]++;
		} else
		    contacts[i][index] = false;
	    }
	}
    }
// contact map overlap
    int crow = protlen * (protlen - 1) / 2;
    unsigned short int *cmo = new unsigned short int[conformers];
#pragma omp parallel for
    for (int j = 0; j < conformers; j++) {
	int native_contacts = 0;
	for (int c = 0; c < crow; c++)
	    native_contacts += (int) (refcontacts[c] & contacts[j][c]);	// if true,true = 1, otherwise = 0
	int div = contacts_positive[j] + refcontacts_len;
	cmo[j] = (int) (1000 * native_contacts * 2.0 / div);
    }


    for (i = 0; i < conformers; i++)
	delete[]contacts[i];
    delete[]contacts;
    delete[]refcontacts;

    return cmo;


}

unsigned short *CMO(float *data, long conformers, int protlen,
		    float cutoff)
{
    long i;
    bool **contacts = new bool *[conformers];
    for (i = 0; i < conformers; i++)
	contacts[i] = new bool[(protlen * (protlen - 1) / 2)];
    int *contacts_positive = new int[conformers];
#pragma omp parallel for
    for (i = 0; i < conformers; i++) {
	contacts_positive[i] = 0;
	for (int j = 0; j < protlen - 1; j++) {
	    int iter = 0;
	    for (int j2 = j + 1; j2 < protlen; j2++) {
		int index = iter + j * (protlen - 1) - j * (j - 1) / 2;
		iter++;
		//    printf("%d %d\n",index,protlen*(protlen-1)/2);
		if (dist(data, i, protlen, j, j2) <= cutoff) {
		    contacts[i][index] = true;
		    contacts_positive[i]++;
		} else
		    contacts[i][index] = false;
	    }
	}
    }
// contact map overlap
    long crow = protlen * (protlen - 1) / 2;
    unsigned short int *cmo =
	new unsigned short int[conformers * (conformers - 1) / 2];
#pragma omp parallel for
    for (i = 0; i < conformers - 1; i++) {
	long iter = 0;
	for (int j = i + 1; j < conformers; j++) {
	    int native_contacts = 0;
	    for (int c = 0; c < crow; c++)
		native_contacts += (int) (contacts[i][c] & contacts[j][c]);	// if true,true = 1, otherwise = 0 
	    long index =
		(long) (iter + i * (conformers - 1) - i * (i - 1) / 2);
	    //printf("%5ld %5d %5d %5d\n",index,(1000*native_contacts*2/(contacts_positive[i]+contacts_positive[j])),contacts_positive[i],contacts_positive[j]);

	    int div = contacts_positive[i] + contacts_positive[j];

	    cmo[index] = (int) (1000 * native_contacts * 2.0 / div);

//      printf("%d %8.3f\n",div,1000*native_contacts*2.0/div);

	    iter++;
	}
    }

    for (i = 0; i < conformers; i++)
	delete[]contacts[i];
    delete[]contacts;

    return cmo;
}



// native contacts only
unsigned short *CMOOneRefn(float *reference, float *data, long conformers,
			   int protlen, float cutoff)
{

    int i;

    int crow = 0;
    bool *refcontacts = new bool[protlen * (protlen - 1) / 2];

    bool **contacts = new bool *[conformers];
    for (i = 0; i < conformers; i++)
	contacts[i] = new bool[(protlen * (protlen - 1) / 2)];
    // reference contact
    for (int j = 0; j < protlen - 1; j++) {
	int iter = 0;
	for (int j2 = j + 1; j2 < protlen; j2++) {
	    int index = iter + j * (protlen - 1) - j * (j - 1) / 2;
	    iter++;
	    if (dist(reference, 0, protlen, j, j2) <= cutoff) {
		crow++;
		refcontacts[index] = true;
	    } else
		refcontacts[index] = false;
	}
    }

    // list contact   

    for (i = 0; i < conformers; i++) {
	for (int j = 0; j < protlen - 1; j++) {
	    int iter = 0;
	    for (int j2 = j + 1; j2 < protlen; j2++) {
		int index = iter + j * (protlen - 1) - j * (j - 1) / 2;
		iter++;
		//    printf("%d %d\n",index,protlen*(protlen-1)/2);
		if (dist(data, i, protlen, j, j2) <= cutoff)
		    contacts[i][index] = true;
		else
		    contacts[i][index] = false;
	    }
	}
    }
// contact map overlap
    unsigned short int *cmo = new unsigned short int[conformers];
#pragma omp parallel for
    for (int j = 0; j < conformers; j++) {
	int native_contacts = 0;
	for (int c = 0; c < (protlen * (protlen - 1) / 2); c++)
	    native_contacts += (int) (refcontacts[c] & contacts[j][c]);	// if true,true  = 1, otherwise = 0 
	cmo[j] = (int) (1000.0 * native_contacts / crow);
    }


    for (i = 0; i < conformers; i++)
	delete[]contacts[i];
    delete[]contacts;
    delete[]refcontacts;

    return cmo;


}
