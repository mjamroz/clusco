unsigned short *CMOOneRef(float *reference, float *data, long conformers,
			  int protlen, float cutoff);
unsigned short *CMO(float *data, long conformers, int protlen,
		    float cutoff);
unsigned short *CMOOneRefn(float *reference, float *data, long conformers,
			   int protlen, float cutoff);
