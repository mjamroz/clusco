#include <math.h>
#ifndef NOOMP
#include <omp.h>
#endif
using namespace std;


inline float dist(float *coordinates, long conformer, int pl, int r2,
		  int r1)
{

    int si = 3 * conformer * pl;
    int r1i = si + 3 * r1;
    int r2i = si + 3 * r2;
    float x = coordinates[r1i] - coordinates[r2i];
    float y = coordinates[r1i + 1] - coordinates[r2i + 1];
    float z = coordinates[r1i + 2] - coordinates[r2i + 2];

    return sqrt(x * x + y * y + z * z);
}


unsigned short *dRMSDOneRef(float *reference, float *data, long conformers,
			    int protlen)
{

    int i;
    float *refdrmsd = new float[protlen * (protlen - 1) / 2];

    float **drmsd = new float *[conformers];
    for (i = 0; i < conformers; i++)
	drmsd[i] = new float[(protlen * (protlen - 1) / 2)];
    // reference distance matrix
    for (int j = 0; j < protlen - 1; j++) {
	int iter = 0;
	for (int j2 = j + 1; j2 < protlen; j2++) {
	    int index = iter + j * (protlen - 1) - j * (j - 1) / 2;
	    iter++;
	    refdrmsd[index] = dist(reference, 0, protlen, j, j2);
	}
    }

    // list dmtx    
#pragma omp parallel for
    for (i = 0; i < conformers; i++) {
	for (int j = 0; j < protlen - 1; j++) {
	    int iter = 0;
	    for (int j2 = j + 1; j2 < protlen; j2++) {
		int index = iter + j * (protlen - 1) - j * (j - 1) / 2;
		iter++;

		drmsd[i][index] = dist(data, i, protlen, j, j2);
	    }
	}
    }
    // DRMSD calculation
    int crow = protlen * (protlen - 1) / 2;
    unsigned short int *drms = new unsigned short int[conformers];
#pragma omp parallel for
    for (int j = 0; j < conformers; j++) {
	float sum = 0;
	for (int c = 0; c < crow; c++) {
	    float ooo = refdrmsd[c] - drmsd[j][c];
	    sum += ooo * ooo;
	}
	drms[j] = (int) (1000 * sqrt(sum / crow));
    }


    for (i = 0; i < conformers; i++)
	delete[]drmsd[i];
    delete[]drmsd;
    delete[]refdrmsd;

    return drms;


}

unsigned short *dRMSD(float *data, long conformers, int protlen)
{
    int i;
    float **dmtx = new float *[conformers];
    for (i = 0; i < conformers; i++)
	dmtx[i] = new float[(protlen * (protlen - 1) / 2)];
#pragma omp parallel for
    for (i = 0; i < conformers; i++) {
	for (int j = 0; j < protlen - 1; j++) {
	    int iter = 0;
	    for (int j2 = j + 1; j2 < protlen; j2++) {
		int index = iter + j * (protlen - 1) - j * (j - 1) / 2;
		iter++;
		dmtx[i][index] = dist(data, i, protlen, j, j2);
	    }
	}
    }
// drmsd
    int crow = protlen * (protlen - 1) / 2;
    unsigned short int *drms =
	new unsigned short int[conformers * (conformers - 1) / 2];
#pragma omp parallel for
    for (i = 0; i < conformers - 1; i++) {
	long iter = 0;
	for (int j = i + 1; j < conformers; j++) {
	    float sum = 0;
	    for (int c = 0; c < crow; c++) {
		float ooo = dmtx[i][c] - dmtx[j][c];
		sum += ooo * ooo;
	    }
	    long index =
		(long) (iter + i * (conformers - 1) - i * (i - 1) / 2);
	    drms[index] = (int) (1000 * sqrt(sum / crow));
	    iter++;
	}
    }

    for (i = 0; i < conformers; i++)
	delete[]dmtx[i];
    delete[]dmtx;

    return drms;
}
