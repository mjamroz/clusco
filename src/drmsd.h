unsigned short *dRMSD(float *data, long conformers, int protlen);
unsigned short *dRMSDOneRef(float *reference, float *data, long conformers,
			    int protlen);
