#include <stdio.h>


#include "cluster_light.h"
#include "hierc.h"
//using namespace std;
extern "C" Node * treecluster(int nrows, float **data, char method,
			      float **distmatrix);
extern "C" void cuttree(int nelements, Node * tree, int nclusters,
			int *clusterid);

int *hierarchicalClustering(float **data, char clust_type, int conformers,
			    int k)
{
    float **dm = NULL;
    Node *tree = treecluster(conformers, data, clust_type, dm);
    int *clusterid = new int[conformers];	//(int*)malloc(conformers*sizeof(int));
    cuttree(conformers, tree, k, clusterid);
    delete tree;
    return clusterid;
}
