//      main.c
//      
//      Copyright 2012 Michal Jamroz <jamroz@chem.uw.edu.pl>
//      
//      This program is free software; you can redistribute it and/or modify
//      it under the terms of the GNU General Public License as published by
//      the Free Software Foundation; either version 2 of the License, or
//      (at your option) any later version.
//      
//      This program is distributed in the hope that it will be useful,
//      but WITHOUT ANY WARRANTY; without even the implied warranty of
//      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//      GNU General Public License for more details.
//      
//      You should have received a copy of the GNU General Public License
//      along with this program; if not, write to the Free Software
//      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
//      MA 02110-1301, USA.
//      
//      


#include <stdio.h>
#include <cmath>
#include <string>
#include <iostream>
#include <time.h>		// debug
#include <tclap/CmdLine.h>
//#include <omp.h>
#include <float.h>
#include <regex.h>

#include "rmsd.h"
#include "pdb_parser.h"
#include "cmo.h"
#include "gdt.h"
#include "drmsd.h"
#ifdef GPU
#include "rmsd_gpu.h"
#endif
//#include "cluster-1.50/src/cluster.h"
#include "utils.h"
#include "clustering.h"
#include "hierc.h"
using namespace std;
double diffclock(clock_t clock1, clock_t clock2)
{
    double diffticks = clock1 - clock2;
    double diffms = (diffticks * 10) / CLOCKS_PER_SEC;
    return diffms;
}

int main(int argc, char **argv)
{

    try {

	TCLAP::
	    CmdLine
	    cmd("Clustering and Comparison of Protein Models / GPL", ' ',
		"0.2");
	TCLAP::ValueArg < string > traArg("t", "trajectory",
					  "Multimodel file (PDB: MODEL .... ENDMDL)",
					  true, "NULL",
					  "trajectory filename");
	TCLAP::ValueArg < string > listArg("l", "list", "List of files",
					   true, "NULL", "list filename");

	cmd.xorAdd(listArg, traArg);
	TCLAP::ValueArg < string > referenceArg("e", "reference",
						"Reference model (PDB file). Without this flag, program computes all-with-all",
						false, "NULL",
						"reference pdb filename");
	TCLAP::ValueArg < string > outputArg("o", "output",
					     "Output filename. Save results here.",
					     false, "NULL",
					     "output filename");
	cmd.add(referenceArg);
	cmd.add(outputArg);
	TCLAP::SwitchArg allatomSwitch("a", "allatom",
				       "Compute scores on all atoms (default: only C-alpha atoms)",
				       cmd, false);
	TCLAP::SwitchArg trafSwitch("c", "cabstraf",
				    "Multimodel TRAF (CABS) file format (default: PDB file format)",
				    cmd, false);
	TCLAP::ValueArg < float >cutoffArg("d", "cutoff",
					   "Cutoff for Contact Map Overlap (default 3.8A)",
					   false, 3.8, "cutoff");
	cmd.add(cutoffArg);

	vector < string > allowed;
	allowed.push_back("rmsd");
#ifdef GPU
	allowed.push_back("rmsdGPU");
#endif
	allowed.push_back("drmsd");

	allowed.push_back("gdt");
	allowed.push_back("gdtExt");

	allowed.push_back("tmscore");
	allowed.push_back("maxsub");
	allowed.push_back("CMO");
	allowed.push_back("CMOn");
	TCLAP::ValuesConstraint < string > scoreVals(allowed);
	TCLAP::ValueArg < string > scoreArg("s", "score",
					    "Select score for computation",
					    true, "rmsd", &scoreVals);
	cmd.add(scoreArg);
	TCLAP::UnlabeledMultiArg < string > clust("z",
						  "clustering options=\nK-Means: 0 OR 0 <K> (if only 0: K=3)\nHierarchical, pairwise single-linkage: 1 OR 1 <K>\nHierarchical, pairwise maximum-linkage: 2 OR 2 <K>\nHierarchical, pairwise average-linkage: 3 OR 3 <K>\n",
						  false,
						  "clustering options (only with -l flag)");
	cmd.add(clust);

	// Parse the argv array.
	cmd.parse(argc, argv);

	// Get the value parsed by each arg. 
	bool allatom = allatomSwitch.getValue();
	bool traf = trafSwitch.getValue();
	string score = scoreArg.getValue();
	string list = listArg.getValue();
	string trafilename = traArg.getValue();
	string outputfilename = outputArg.getValue();
	string reffilename = referenceArg.getValue();
	vector < string > clustering = clust.getValue();
	float cutoff = cutoffArg.getValue();
	float *data = NULL;	//init array of coordinates
	unsigned short *results_cpu = NULL;
	int arraysize, protlen;
	long conformers;	// init variables
	bool brmsd = false;


	printf("loading coordinates, %s\n", list.c_str());
	if (list != "NULL") {
	    data =
		loadDataFromFiles(&arraysize, &protlen, &conformers,
				  (char *) list.c_str(), !allatom);

	} else if (trafilename != "NULL") {


	    if (traf)
		data =
		    loadDataFromTRAF(&arraysize, &protlen, &conformers,
				     (char *) trafilename.c_str());

	    else {


		data =
		    loadDataFromMultimodelPDB2(&arraysize, &protlen,
					       &conformers,
					       (char *) trafilename.
					       c_str(), !allatom);
	    }
	}

	//double beginw = omp_get_wtime();
	if (reffilename == "NULL") {


	    long val = conformers * (conformers - 1) / 2;
	    printf("calculating score, %ld values, memory size %5ld kB\n",
		   val,
		   (long int) sizeof(unsigned int) * conformers *
		   (conformers - 1) / 2 / 1024);
	    // select score , calc all to all

	    if (score == "rmsd") {
		results_cpu = rmsdCPU(data, conformers, protlen);
		brmsd = true;
	    } else if (score == "gdt")
		results_cpu = gdtCPU(data, conformers, protlen, 1);
	    else if (score == "drmsd") {
		results_cpu = dRMSD(data, conformers, protlen);
		brmsd = true;
	    }
#ifdef GPU
	    else if (score == "rmsdGPU") {
		results_cpu = calc_rmsd(data, conformers, protlen);

		brmsd = true;
	    }
#endif
	    else if (score == "tmscore")
		results_cpu = gdtCPU(data, conformers, protlen, 2);
	    else if (score == "maxsub")
		results_cpu = gdtCPU(data, conformers, protlen, 3);
	    else if (score == "CMO")
		results_cpu = CMO(data, conformers, protlen, cutoff);
	    //case "all":
	    //double endw = omp_get_wtime();
	    delete[]data;
	    //cout << "Calculation time elapsed: " << (endw -
						     //beginw) << " s" <<
		//endl;
	    // save results
	    if (outputfilename != "NULL") {
		printf("saving results\n");
		saveResults((char *) outputfilename.c_str(), results_cpu,
			    conformers, false);
	    }

#include "main_clustering.c"

	}
// ONLY CALCULATE SCORE AND SAVE    
	else {
	    printf("calculating score, %ld values, memory size %5ld kB\n",
		   conformers, sizeof(unsigned int) * conformers / 1024);

	    // select score , calc to reference
	    int atoms = countAtoms((char *) reffilename.c_str(), !allatom);
	    float *reference = new float[atoms * 3];

	    readPDB((char *) reffilename.c_str(), reference, !allatom);
	    centerize(reference, atoms, 1);

	    if (score == "rmsd")
		results_cpu =
		    rmsdCPUOneReference(reference, data, conformers,
					atoms);
	    else if (score == "gdt")
		results_cpu =
		    gdtCPUOneReference(reference, data, conformers, atoms,
				       1);
	    else if (score == "drmsd")
		results_cpu =
		    dRMSDOneRef(reference, data, conformers, atoms);
	    else if (score == "gdtExt")
		results_cpu =
		    gdtCPUOneReferenceExt(reference, data, conformers,
					  atoms, 1);
#ifdef GPU
	    else if (score == "rmsdGPU")
		results_cpu =
		    calc_rmsdOneRef(reference, data, conformers, atoms);
#endif
	    else if (score == "tmscore")
		results_cpu =
		    gdtCPUOneReference(reference, data, conformers, atoms,
				       2);
	    else if (score == "maxsub")
		results_cpu =
		    gdtCPUOneReference(reference, data, conformers, atoms,
				       3);
	    else if (score == "CMO")
		results_cpu =
		    CMOOneRef(reference, data, conformers, atoms, cutoff);
	    else if (score == "CMOn")
		results_cpu =
		    CMOOneRefn(reference, data, conformers, atoms, cutoff);
	    //case "all":

	    delete[]data;
	    // save results
	    if (outputfilename != "NULL") {
		printf("saving results..\n");
		saveResults((char *) outputfilename.c_str(), results_cpu,
			    conformers, true);

	    }

	}



    } catch(TCLAP::ArgException & e)	// catch any exceptions
    {
	cerr << "error: " << e.error() << " for arg " << e.argId() << endl;
    }




    return 0;
}
