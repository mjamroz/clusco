// CLUSTERING SECTION 
//double beginw = omp_get_wtime();
int csize = clustering.size();
if (csize > 0) {
    string header = "# Score: " + score + "\n";
    ostringstream conf;
    conf << conformers;

    if (trafilename != "NULL")
	header +=
	    "# Filename: " + trafilename + " (conformers: " + conf.str() +
	    ")\n";
    else
	header +=
	    "# Filename: " + list + " (conformers: " + conf.str() + ")\n";
    float **results_float;
    results_float = new float *[conformers];
    for (int i = 0; i < conformers; i++)
	results_float[i] = new float[conformers];
    // 2 d for clustering
    reshapeArray(results_cpu, results_float, conformers, brmsd);
    //   saveResults2D((char *)"testy", results_float,conformers);
    delete[]results_cpu;
    int *membership = NULL;
    float t = 0.01;
    int k = 10;
//        saveResults2D((char *)"input.txt",  results_float, conformers)    ;
    if (clustering[0] == "0") {
	printf("K-Means clustering ");


	if (csize == 1) {	// default clustering values
	    t = 0.01;
	    k = 3;
	} else if (csize == 2) {	// threshold and K as user input
	    t = 0.01;		//atof(clustering[1].c_str());
	    k = atoi(clustering[1].c_str());
	} else {
	    printf
		(": I don't understand your K-means clustering options. Clustering with default threshold and K.\n");
	}

/*                if (k==-1) {
                    float density=100.0,density_p=FLT_MAX;
                    for (int i=1;i<50+1;i++) {
                        
                            membership = kMeansClustering(results_float, (int)conformers,t,i);
                            float * as = avgScore(results_float, membership, i, conformers);
                            delete [] membership;
                            density=0;
                            for (int e=0;e<i;e++)
                                density += as[e];
                            density /= i;
                            delete [] as;
                            //cout << i << " "<< density << " "<<abs(density-density_p)<<endl;
                            if (density<1e-4 || abs(density-density_p)<=0.05 || i==conformers) { 
                                k=i;
                                break;
                            }
                            density_p = density;
                            
                    }
                    
                }
  */
	printf("with threshold = %5.3f and number of clusters = %d\n", t,
	       k);
	ostringstream ss;
	ss << "# K-Means clustering t=" << t << ", K=" << k << endl;
	header += ss.str();

	membership =
	    kMeansClustering2(results_float, (int) conformers, t, k);

    } else if (clustering[0] == "1") {	// hierarchical, pairwise single-linkage clustering

	printf("Hierarchical, pairwise single-linkage ");
	if (csize == 1)		// default clustering values
	    k = 3;

	else if (csize == 2)	// threshold and K as user input
	    k = atoi(clustering[1].c_str());

	else
	    printf
		(": I don't understand your hierarchical clustering options. Clustering with default K.\n");

	if (k == -1) {
	    float density = 100.0, density_p = FLT_MAX;
	    for (int i = 1; i < 50 + 1; i++) {

		membership =
		    hierarchicalClustering(results_float, 's', conformers,
					   i);
		float *as =
		    avgScore(results_float, membership, i, conformers);
		delete[]membership;
		density = 0;
		for (int e = 0; e < i; e++)
		    density += as[e];
		density /= i;
		delete[]as;
		//cout << i << " "<< density << " "<<abs(density-density_p)<<endl;
		if (density < 1e-4 || abs(density - density_p) <= 0.05
		    || i == conformers) {
		    k = i;
		    break;
		}
		density_p = density;

	    }

	}
	ostringstream ss;
	ss << "# Hierarchical clustering, pairwise single-linkage, K=" << k
	    << endl;
	header += ss.str();
	membership =
	    hierarchicalClustering(results_float, 's', conformers, k);
    } else if (clustering[0] == "2") {	// hierarchical, pairwise maximum- (or complete-) linkage clustering
	printf("Hierarchical, pairwise maximum- (or complete-) linkage ");

	if (csize == 1)		// default clustering values
	    k = 3;

	else if (csize == 2)	// threshold and K as user input
	    k = atoi(clustering[1].c_str());

	else
	    printf
		(": I don't understand your hierarchical clustering options. Clustering with default K.\n");

	if (k == -1) {
	    float density = 100.0, density_p = FLT_MAX;
	    for (int i = 1; i < 50 + 1; i++) {

		membership =
		    hierarchicalClustering(results_float, 'm', conformers,
					   i);
		float *as =
		    avgScore(results_float, membership, i, conformers);
		delete[]membership;
		density = 0;
		for (int e = 0; e < i; e++)
		    density += as[e];
		density /= i;
		delete[]as;
		//cout << i << " "<< density << " "<<abs(density-density_p)<<endl;
		if (density < 1e-4 || abs(density - density_p) <= 0.05
		    || i == conformers) {
		    k = i;
		    break;
		}
		density_p = density;

	    }

	}

	ostringstream ss;
	ss <<
	    "# Hierarchical clustering, pairwise maximum- (or complete-) linkage, K="
	    << k << endl;
	header += ss.str();
	membership =
	    hierarchicalClustering(results_float, 'm', conformers, k);
    } else if (clustering[0] == "3") {	// hierarchical, pairwise average-linkage
	printf("Hierarchical, pairwise average-linkage ");

	if (csize == 1)		// default clustering values
	    k = 3;

	else if (csize == 2)	// threshold and K as user input
	    k = atoi(clustering[1].c_str());

	else
	    printf
		(": I don't understand your hierarchical clustering options. Clustering with default K.\n");

	if (k == -1) {
	    float density = 100.0, density_p = FLT_MAX;
	    for (int i = 1; i < 50 + 1; i++) {

		membership =
		    hierarchicalClustering(results_float, 'a', conformers,
					   i);
		float *as =
		    avgScore(results_float, membership, i, conformers);
		delete[]membership;
		density = 0;
		for (int e = 0; e < i; e++)
		    density += as[e];
		density /= i;
		delete[]as;
		//cout << i << " "<< density << " "<<abs(density-density_p)<<endl;
		if (density < 1e-4 || abs(density - density_p) <= 0.05
		    || i == conformers) {
		    k = i;
		    break;
		}
		density_p = density;

	    }

	}

	ostringstream ss;
	ss << "# Hierarchical clustering, pairwise average-linkage, K=" <<
	    k << endl;
	header += ss.str();
	membership =
	    hierarchicalClustering(results_float, 'a', conformers, k);
    } else {
	printf("Wrong clustering method\n exit\n");
	return 1;
    }
    //double endw = omp_get_wtime();
    //cout << "clustering time elapsed: " << (endw - beginw) << " s" << endl;

    if (list != "NULL")
	saveClusteringResults((char *) list.c_str(), membership, false, k,
			      conformers, (char *) header.c_str(),
			      results_float);
    else
	saveClusteringResults((char *) trafilename.c_str(), membership,
			      true, k, conformers, (char *) header.c_str(),
			      results_float);

    for (int i = 0; i < conformers; i++)
	delete[]results_float[i];	// delete 2d array 
    delete[]results_float;
}
