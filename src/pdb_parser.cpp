#include <stdio.h>
#include <iostream>

#include <string>
#include <fstream>
#include <stdlib.h>
#include <sstream>
#include <istream>
#include <regex.h>
#ifndef NOOMP
#include <omp.h>
#endif
#include "pdb_parser.h"
using namespace std;

void centerize(float *data, int chain_len, int models)
{

// int i,j;
// float x_cm,y_cm,z_cm;
//#pragma omp parallel for
    float x_cm, y_cm, z_cm;

    for (int i = 0; i < models; i++) {

	x_cm = y_cm = z_cm = 0.0;

	for (int j = i * 3 * chain_len; j < (i + 1) * 3 * chain_len;
	     j += 3) {
	    x_cm += data[j];
	    y_cm += data[j + 1];
	    z_cm += data[j + 2];
	}

	x_cm /= chain_len;
	y_cm /= chain_len;
	z_cm /= chain_len;

	for (int j = i * 3 * chain_len; j < (i + 1) * 3 * chain_len;
	     j += 3) {
	    data[j] -= x_cm;
	    data[j + 1] -= y_cm;
	    data[j + 2] -= z_cm;
	}
    }

}

void
getXYZ2(char *xc, char *yc, char *zc, char *line, float *x, float *y,
	float *z)
{
    for (int i = 0; i < 8; i++) {
	xc[i] = line[30 + i];
	yc[i] = line[38 + i];
	zc[i] = line[46 + i];

    }
    *x = strtof(xc, NULL);
    *y = strtof(yc, NULL);
    *z = strtof(zc, NULL);

}

inline void getXYZ(char *line, float *x, float *y, float *z)
{
    char *xc = new char[9];
    char *yc = new char[9];
    char *zc = new char[9];
    for (int i = 0; i < 8; i++) {
	xc[i] = line[30 + i];
	yc[i] = line[38 + i];
	zc[i] = line[46 + i];

    }

    xc[8] = '\0';
    yc[8] = '\0';
    zc[8] = '\0';
    *x = strtof(xc, NULL);
    *y = strtof(yc, NULL);
    *z = strtof(zc, NULL);

    delete[]xc;
    delete[]yc;
    delete[]zc;
}

int countAtoms(char *filename, bool calphaOnly)
{
    regex_t allatoms, caatoms;
    regcomp(&allatoms, "^ATOM", REG_EXTENDED);
    regcomp(&caatoms, "^ATOM.........CA", REG_EXTENDED);
    int err;
    int counter = 0;
    string s;
    ifstream myfile;
    myfile.open(filename, ifstream::in);
    while (getline(myfile, s)) {
	const char *b = s.c_str();
	if ((err = regexec(&allatoms, b, 0, NULL, 0)) == 0) {
	    if (!calphaOnly)
		counter++;
	    else if ((err = regexec(&caatoms, b, 0, NULL, 0)) == 0
		     && calphaOnly)
		counter++;

	}


    }
    myfile.close();
    regfree(&allatoms);
    regfree(&caatoms);
    return counter;

}


void readPDB(char *filename, float *data, bool calphaOnly)
{
    int i = 0;
    regex_t allatoms, caatoms;
    regcomp(&allatoms, "^ATOM", REG_EXTENDED);
    regcomp(&caatoms, "^ATOM.........CA", REG_EXTENDED);

    int err;
    FILE *pFile;
    float x, y, z;
    char line[80];
    char *xc = new char[8];
    char *yc = new char[8];
    char *zc = new char[8];

    pFile = fopen(filename, "r");

    while (fgets(line, 80, pFile)) {
	if ((err = regexec(&allatoms, line, 0, NULL, 0)) == 0) {
	    if (!calphaOnly) {
		getXYZ(line, &x, &y, &z);
		data[i++] = x;
		data[i++] = y;
		data[i++] = z;


	    } else if ((err = regexec(&caatoms, line, 0, NULL, 0)) == 0
		       && calphaOnly) {
		getXYZ(line, &x, &y, &z);
		data[i++] = x;
		data[i++] = y;
		data[i++] = z;

	    }

	}

    }
    delete[]xc;
    delete[]yc;
    delete[]zc;
    regfree(&allatoms);
    regfree(&caatoms);
    fclose(pFile);

}

float *loadDataFromMultimodelPDB(int *size, int *chainlength, long *models,
				 char *filename, bool calphaOnly)
{
    // format: MODEL ... ENDMDL 
    regex_t allatoms, caatoms, model;
    regcomp(&allatoms, "^ATOM", REG_EXTENDED);
    regcomp(&caatoms, "^ATOM.........CA", REG_EXTENDED);
    regcomp(&model, "^MODEL", REG_EXTENDED);

    int err;

    ifstream trajectory;
    string line, ax, ay, az;

    trajectory.open(filename, ifstream::in);
    int number_of_atoms = 0;
    int number_of_models = 0;

    while (getline(trajectory, line)) {
	const char *b = line.c_str();
	if ((err = regexec(&model, b, 0, NULL, 0)) == 0)
	    number_of_models++;

	if (number_of_models == 1) {

	    if ((err = regexec(&caatoms, b, 0, NULL, 0)) == 0
		&& calphaOnly) {
		number_of_atoms++;
	    } else if ((err = regexec(&allatoms, b, 0, NULL, 0)) == 0
		       && !calphaOnly) {
		number_of_atoms++;
	    }

	}

    }

    int array_size = number_of_atoms * number_of_models * 3;

    *chainlength = number_of_atoms;
    *models = number_of_models;
    *size = array_size;


    trajectory.clear();
    trajectory.seekg(0, ios::beg);
    float *data = NULL;
    data = new float[array_size];
    int acounter = 0;


    while (getline(trajectory, line)) {
	const char *b = line.c_str();
	if ((err = regexec(&allatoms, b, 0, NULL, 0)) == 0) {

	    if (!calphaOnly) {
		ax = line.substr(30, 8);
		ay = line.substr(38, 8);
		az = line.substr(46, 8);
		data[acounter++] = atof(ax.c_str());
		data[acounter++] = atof(ay.c_str());
		data[acounter++] = atof(az.c_str());

	    }


	    else if ((err = regexec(&caatoms, b, 0, NULL, 0)) == 0
		     && calphaOnly) {
		ax = line.substr(30, 8);
		ay = line.substr(38, 8);
		az = line.substr(46, 8);
		data[acounter++] = atof(ax.c_str());
		data[acounter++] = atof(ay.c_str());
		data[acounter++] = atof(az.c_str());

	    }
	}




    }

    regfree(&allatoms);
    regfree(&caatoms);
    regfree(&model);
    trajectory.close();
    centerize(data, number_of_atoms, number_of_models);
    return data;

}


float *loadDataFromMultimodelPDB2(int *size, int *chainlength,
				  long *models, char *filename,
				  bool calphaOnly)
{
    // format: MODEL ... ENDMDL 
    regex_t allatoms, caatoms, model;
    regcomp(&allatoms, "^ATOM", REG_EXTENDED);
    regcomp(&caatoms, "^ATOM.........CA", REG_EXTENDED);
    regcomp(&model, "^MODEL", REG_EXTENDED);

    int err;
    FILE *pFile;
    //  char buffer[BUFSIZ];
    char line[78];
    pFile = fopen(filename, "r");
    int number_of_atoms = 0;
    int number_of_models = 0;

    while (fgets(line, 78, pFile)) {
	const char *b = line;	//.c_str();
	if ((err = regexec(&model, b, 0, NULL, 0)) == 0)
	    number_of_models++;

	if (number_of_models == 1) {

	    if ((err = regexec(&caatoms, b, 0, NULL, 0)) == 0
		&& calphaOnly) {
		number_of_atoms++;
	    } else if ((err = regexec(&allatoms, b, 0, NULL, 0)) == 0
		       && !calphaOnly) {
		number_of_atoms++;
	    }

	}

    }

    long array_size = number_of_atoms * number_of_models * 3;

    *chainlength = number_of_atoms;
    *models = number_of_models;
    *size = array_size;


    pFile = fopen(filename, "r");
    //setbuf(pFile,buffer);
    float *data = NULL;

    int acounter = 0;
    float x, y, z;

    data = new float[array_size];
    while (fgets(line, 78, pFile)) {
	//const char* b = line;//.c_str();
	if ((err = regexec(&allatoms, line, 0, NULL, 0)) == 0) {

	    if (!calphaOnly) {
		getXYZ(line, &x, &y, &z);
		data[acounter++] = x;
		data[acounter++] = y;
		data[acounter++] = z;
	    }
	    else if (calphaOnly
		     && (err = regexec(&caatoms, line, 0, NULL, 0)) == 0) {
		getXYZ(line, &x, &y, &z);
		data[acounter++] = x;
		data[acounter++] = y;
		data[acounter++] = z;
	    }
	}




    }

    regfree(&allatoms);
    regfree(&caatoms);
    regfree(&model);
    fclose(pFile);
    //trajectory.close();
    centerize(data, number_of_atoms, number_of_models);

    return data;

}


float *loadDataFromFiles(int *size, int *chainlength, long *models,
			 char *filename, bool calphaOnly)
{



    regex_t allatoms, caatoms;
    regcomp(&allatoms, "^ATOM", REG_EXTENDED);
    regcomp(&caatoms, "^ATOM.........CA", REG_EXTENDED);

    // get number of atoms and number of lines
    int nlines = 1;
    char firstfname[256], pdb[256];

    ifstream lists;
    lists.open(filename, ifstream::in);
    lists.getline(firstfname, 256);
    int atoms = countAtoms(firstfname, calphaOnly);
    *chainlength = atoms;
    string line;
    while (getline(lists, line)) {
	if (!line.empty())
	    nlines++;
    }



    *models = nlines;
    int arraysize = atoms * 3 * nlines;
    *size = arraysize;
    float *data = new float[arraysize]; // flat array of size N_models * N_atoms_per_model * 3

    int dataiter = 0, i;
    // read lists line by line and load into data array
    string pdb2;

    lists.clear();
    lists.seekg(0, ios::beg);	//rewind to  beg
    float *datatmp = new float[atoms * 3]; // array of single model length (N_atoms_per_model*3)


    // read list of pdb models
    while (lists.good()) {
	lists.getline(pdb, 256);
	pdb2 = pdb;
	if (!pdb2.empty()) {
            //fetch x ,y,z coordinates into datatmp of size N_atoms_per_model*3
	    readPDB(pdb, datatmp, calphaOnly);
	    for (i = 0; i < atoms * 3; i++)
		data[dataiter++] = datatmp[i]; // insert that model in the data array, one model after another

	}
    }


    delete[]datatmp;
    lists.close();


    centerize(data, atoms, nlines);

    return data;
}

float *loadDataFromTRAF(int *size, int *chainlength, long *models,
			char *filename)
{
    ifstream traf;
    traf.open(filename, ifstream::in);
    regex_t header;
    regcomp(&header, "[.]", REG_EXTENDED);	// TRAF header contains dot 
    int number_of_models = 0, number_of_resid = 0, err, vx, vy, vz;
    string s, chl;

// get chain length
    getline(traf, s);		//first line is always header
    chl = s.substr(6, 6);
    number_of_resid = atoi(chl.c_str()) - 2;
//rewind
    traf.clear();
    traf.seekg(0, ios::beg);

// get trajectory length
    while (getline(traf, s)) {
	const char *b = s.c_str();
	if ((err = regexec(&header, b, 0, NULL, 0)) == 0)
	    number_of_models++;
    }

//rewind
    traf.clear();
    traf.seekg(0, ios::beg);

// get coordinates
    int *tmpmodel = NULL;
    float *coordinates = NULL;
    coordinates = new float[number_of_models * number_of_resid * 3];
    int c_iter = 0;
    int iter = 666;
    const int tmplen = 3 * (number_of_resid + 2);
    tmpmodel = new int[tmplen];
    while (getline(traf, s)) {
	const char *b = s.c_str();
	if ((err = regexec(&header, b, 0, NULL, 0)) == 0) {

	    // save previous model to coordinates array, omit dummy atoms and convert to float
	    if (iter == tmplen) {
		for (int i = 3; i < tmplen - 3; i++) {
		    coordinates[c_iter++] = 0.61 * tmpmodel[i];
		}
	    }

	    iter = 0;
	} else {
	    istringstream iss(s, istringstream::in);
	    while (iss >> vx >> vy >> vz) {
		tmpmodel[iter++] = vx;
		tmpmodel[iter++] = vy;
		tmpmodel[iter++] = vz;
	    }
	}
    }				// end of while loop
    traf.close();

    // last model
    for (int i = 3; i < tmplen - 3; i++)
	coordinates[c_iter++] = 0.61 * tmpmodel[i];
    delete[]tmpmodel;
    regfree(&header);


    *size = number_of_models * number_of_resid * 3;
    *chainlength = number_of_resid;
    *models = number_of_models;

    centerize(coordinates, number_of_resid, number_of_models);

    return coordinates;
}
