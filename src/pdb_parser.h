float *loadDataFromFiles(int *size, int *chainlength, long *models,
			 char *filename, bool calphaOnly);
float *loadDataFromMultimodelPDB(int *size, int *chainlength, long *models,
				 char *filename, bool calphaOnly);
float *loadDataFromMultimodelPDB2(int *size, int *chainlength,
				  long *models, char *filename,
				  bool calphaOnly);
float *loadDataFromTRAF(int *size, int *chainlength, long *models,
			char *filename);
int countAtoms(char *filename, bool calphaOnly);
void readPDB(char *filename, float *data, bool calphaOnly);
void centerize(float *data, int chain_len, int models);
