#include <iostream>
#include <math.h>
#include <stdlib.h>
#ifndef NOOMP
#include <omp.h>
#endif
#include <stdio.h>
#include "rmsd.h"

using namespace std;

unsigned short *rmsdCPUOneReference(float *reference, float *arr,
				    long conformers, int protlen)
{

    unsigned short *result = NULL;
    result = new unsigned short[conformers];



    float covmat0, covmat1, covmat2, covmat3, covmat4, covmat5, covmat6,
	covmat7, covmat8, Rg;
    float s_i_y, s_i_z, s_i_x, s_j_x, s_j_y, s_j_z;
    float invlen = 1.0 / protlen;

#pragma omp parallel for  private(covmat0,covmat1,covmat2,covmat3,covmat4,covmat5,covmat6,covmat7,covmat8,Rg,s_i_y,s_i_z,s_i_x,s_j_x,s_j_y,s_j_z) shared(invlen)
    for (int j = 0; j < conformers; j++) {

	int s_j = 3 * protlen * j;
	covmat0 = covmat1 = covmat2 = covmat3 = covmat4 = covmat5 =
	    covmat6 = covmat7 = covmat8 = Rg = 0.0;
	for (int a = 0; a < 3 * protlen - 2; a += 3) {

	    s_i_x = reference[a];
	    s_i_y = reference[a + 1];
	    s_i_z = reference[a + 2];

	    s_j_x = arr[s_j + a];
	    s_j_y = arr[s_j + a + 1];
	    s_j_z = arr[s_j + a + 2];

	    covmat0 += s_i_x * s_j_x;	//arr[s_i+a]* arr[s_j+a];
	    covmat1 += s_i_y * s_j_x;	//arr[s_i+a+1]* arr[s_j+a];
	    covmat2 += s_i_z * s_j_x;	//arr[s_i+a+2]* arr[s_j+a];

	    //printf("%d %7.3f %7.3f %7.3f %d %d\n",j,s_j_x,s_j_y,s_j_z,s_j+a,3*protlen+j);
	    covmat3 += s_i_x * s_j_y;	//arr[s_i+a]* arr[s_j+a+1];
	    covmat4 += s_i_y * s_j_y;	//arr[s_i+a+1]* arr[s_j+a+1];
	    covmat5 += s_i_z * s_j_y;	//arr[s_i+a+2]* arr[s_j+a+1];
	    covmat6 += s_i_x * s_j_z;	//arr[s_i+a]* arr[s_j+a+2];
	    covmat7 += s_i_y * s_j_z;	//arr[s_i+a+1]* arr[s_j+a+2];
	    covmat8 += s_i_z * s_j_z;	//arr[s_i+a+2]* arr[s_j+a+2];
	    Rg += s_i_x * s_i_x + s_j_x * s_j_x;	//arr[s_i+a]*arr[s_i+a]+ arr[s_j+a]*arr[s_j+a];
	    Rg += s_i_y * s_i_y + s_j_y * s_j_y;	//arr[s_i+a+1]*arr[s_i+a+1]+ arr[s_j+a+1]*arr[s_j+a+1];
	    Rg += s_i_z * s_i_z + s_j_z * s_j_z;	//arr[s_i+a+2]*arr[s_i+a+2]+ arr[s_j+a+2]*arr[s_j+a+2];
	}

//      printf("%5d %10.5f %10.5f %10.5f %10.5f %10.5f %10.5f\n",j,covmat0,covmat1,covmat2,covmat3,covmat4,covmat5);    
	Rg *= invlen;
	covmat0 *= invlen;
	covmat1 *= invlen;
	covmat2 *= invlen;
	covmat3 *= invlen;
	covmat4 *= invlen;
	covmat5 *= invlen;
	covmat6 *= invlen;
	covmat7 *= invlen;
	covmat8 *= invlen;



	// compute covariance matrix determinant 
	double determinant =
	    covmat0 * (covmat4 * covmat8 - covmat5 * covmat7) -
	    covmat1 * (covmat3 * covmat8 - covmat5 * covmat6) +
	    covmat2 * (covmat3 * covmat7 - covmat4 * covmat6);

// check det sign
	float sign = 1.0;
	if (determinant < 0.0)
	    sign = -1.0;

	// square of covariance matrix, R2   
	double r0 =
	    covmat0 * covmat0 + covmat3 * covmat3 + covmat6 * covmat6;
	double r1 =
	    covmat0 * covmat1 + covmat3 * covmat4 + covmat6 * covmat7;
	double r2 =
	    covmat0 * covmat2 + covmat3 * covmat5 + covmat6 * covmat8;
	double r4 =
	    covmat1 * covmat1 + covmat4 * covmat4 + covmat7 * covmat7;
	double r5 =
	    covmat1 * covmat2 + covmat4 * covmat5 + covmat7 * covmat8;
	double r8 =
	    covmat2 * covmat2 + covmat5 * covmat5 + covmat8 * covmat8;


	// calc eigenvalues 
	double root0, root1, root2;
	const double inv3 = 0.333333333333333;	//1.0/3.0;
	const double root3 = 1.732050807568877294;
	double c0 =
	    r0 * r4 * r8 + 2.0 * r1 * r2 * r5 - r0 * r5 * r5 -
	    r4 * r2 * r2 - r8 * r1 * r1;
	double c1 =
	    r0 * r4 - r1 * r1 + r0 * r8 - r2 * r2 + r4 * r8 - r5 * r5;
	double c2 = r0 + r4 + r8;
	double c2Div3 = c2 * inv3;
	double aDiv3 = (c1 - c2 * c2Div3) * inv3;
	if (aDiv3 > 0.0)
	    aDiv3 = 0.0;
	double mbDiv2 = 0.5 * (c0 + c2Div3 * (2.0 * c2Div3 * c2Div3 - c1));
	double q = mbDiv2 * mbDiv2 + aDiv3 * aDiv3 * aDiv3;
	if (q > 0.0)
	    q = 0.0;

	double magnitude = sqrt(-aDiv3);
	double angle = atan2(sqrt(-1.0 * q), mbDiv2) / 3.0;
	double sn, cs;		//=sin(angle);double cs=cos(angle);
#ifdef __APPLE__
	__sincos(angle, &sn, &cs);
#else
	sincos(angle, &sn, &cs);
#endif

	double magnitudecs = magnitude * cs;
	double magnituderoot = magnitude * root3 * sn;

	root0 = c2Div3 + 2.0 * magnitudecs;
	root1 = c2Div3 - magnitudecs - magnituderoot;
	root2 = c2Div3 - magnitudecs + magnituderoot;

	if (root0 < 1e-6)
	    root0 = 0;
	if (root1 < 1e-6)
	    root1 = 0;
	if (root2 < 1e-6)
	    root2 = 0;

	double minr, maxr, midr;
	minr = min(root0, min(root1, root2));
	maxr = max(root0, max(root1, root2));
	midr =
	    (maxr == root0) ? (minr == root2 ? root1 : root2) : (minr ==
								 root2 ?
								 root1 :
								 root0);

	double dwa = 2.0 * (sqrt(maxr) + sqrt(midr) + sign * sqrt(minr));


	double rms = sqrt(Rg - dwa);
	if (rms < 12e-3)
	    rms = 0.0f;
	result[j] = (unsigned short) (1000 * rms);

    }				// end of j loop

    return result;
}



unsigned short *rmsdCPU(float *arr, long conformers, int protlen)
{

    unsigned long triangleLen = conformers * (conformers - 1) / 2;
    unsigned short *result = NULL;
    result = new unsigned short[triangleLen];

//printf("%d %d %d",triangleLen,conformers,protlen);    
    float covmat0, covmat1, covmat2, covmat3, covmat4, covmat5, covmat6,
	covmat7, covmat8, Rg;
    float s_i_y, s_i_z, s_i_x, s_j_x, s_j_y, s_j_z;
    float invlen = 1.0 / (float) protlen;
#pragma omp parallel for private(covmat0,covmat1,covmat2,covmat3,covmat4,covmat5,covmat6,covmat7,covmat8,Rg,s_i_y,s_i_z,s_i_x,s_j_x,s_j_y,s_j_z) schedule(dynamic)
    for (long idx = 0; idx < conformers - 1; idx++) {
	long iter = 0;
	for (long j = idx + 1; j < conformers; j++) {

	    int s_i = (int) idx * protlen * 3;
	    int s_j = (int) 3 * protlen * j;


	    covmat0 = covmat1 = covmat2 = covmat3 = covmat4 = covmat5 =
		covmat6 = covmat7 = covmat8 = Rg = 0;
	    for (int a = 0; a < protlen * 3 - 2; a += 3) {
		s_i_x = arr[s_i + a];
		s_i_y = arr[s_i + a + 1];
		s_i_z = arr[s_i + a + 2];
		s_j_x = arr[s_j + a];
		s_j_y = arr[s_j + a + 1];
		s_j_z = arr[s_j + a + 2];

		covmat0 += s_i_x * s_j_x;	//arr[s_i+a]* arr[s_j+a];
		covmat1 += s_i_y * s_j_x;	//arr[s_i+a+1]* arr[s_j+a];
		covmat2 += s_i_z * s_j_x;	//arr[s_i+a+2]* arr[s_j+a];
		covmat3 += s_i_x * s_j_y;	//arr[s_i+a]* arr[s_j+a+1];
		covmat4 += s_i_y * s_j_y;	//arr[s_i+a+1]* arr[s_j+a+1];
		covmat5 += s_i_z * s_j_y;	//arr[s_i+a+2]* arr[s_j+a+1];
		covmat6 += s_i_x * s_j_z;	//arr[s_i+a]* arr[s_j+a+2];
		covmat7 += s_i_y * s_j_z;	//arr[s_i+a+1]* arr[s_j+a+2];
		covmat8 += s_i_z * s_j_z;	//arr[s_i+a+2]* arr[s_j+a+2];
		Rg += s_i_x * s_i_x + s_j_x * s_j_x;	//arr[s_i+a]*arr[s_i+a]+ arr[s_j+a]*arr[s_j+a];
		Rg += s_i_y * s_i_y + s_j_y * s_j_y;	//arr[s_i+a+1]*arr[s_i+a+1]+ arr[s_j+a+1]*arr[s_j+a+1];
		Rg += s_i_z * s_i_z + s_j_z * s_j_z;	//arr[s_i+a+2]*arr[s_i+a+2]+ arr[s_j+a+2]*arr[s_j+a+2];
	    }

	    Rg *= invlen;
	    covmat0 *= invlen;
	    covmat1 *= invlen;
	    covmat2 *= invlen;
	    covmat3 *= invlen;
	    covmat4 *= invlen;
	    covmat5 *= invlen;
	    covmat6 *= invlen;
	    covmat7 *= invlen;
	    covmat8 *= invlen;


	    // compute covariance matrix determinant 
	    float determinant =
		covmat0 * (covmat4 * covmat8 - covmat5 * covmat7) -
		covmat1 * (covmat3 * covmat8 - covmat5 * covmat6) +
		covmat2 * (covmat3 * covmat7 - covmat4 * covmat6);
	    // check det sign
	    double sign = 1.0;
	    if (determinant < 0.0)
		sign = -1.0;

	    // square of covariance matrix, R2   
	    double r0 =
		covmat0 * covmat0 + covmat3 * covmat3 + covmat6 * covmat6;
	    double r1 =
		covmat0 * covmat1 + covmat3 * covmat4 + covmat6 * covmat7;
	    double r2 =
		covmat0 * covmat2 + covmat3 * covmat5 + covmat6 * covmat8;
	    double r4 =
		covmat1 * covmat1 + covmat4 * covmat4 + covmat7 * covmat7;
	    double r5 =
		covmat1 * covmat2 + covmat4 * covmat5 + covmat7 * covmat8;
	    double r8 =
		covmat2 * covmat2 + covmat5 * covmat5 + covmat8 * covmat8;

	    // calc eigenvalues 
	    double root0, root1, root2;
	    const double inv3 = 0.333333333333333333;	//1.0/3.0;
	    const double root3 = 1.732050807568877294;
	    double r55 = r5 * r5;
	    double r22 = r2 * r2;
	    double r11 = r1 * r1;
	    double r04 = r0 * r4;
	    double c0 =
		r04 * r8 + 2.0 * r1 * r2 * r5 - r0 * r55 - r4 * r22 -
		r8 * r11;
	    double c1 = r04 - r11 + r0 * r8 - r22 + r4 * r8 - r55;
	    double c2 = r0 + r4 + r8;
	    double c2Div3 = c2 * inv3;
	    double aDiv3 = (c1 - c2 * c2Div3) * inv3;
	    if (aDiv3 > 0.0)
		aDiv3 = 0.0;
	    double mbDiv2 =
		0.5 * (c0 + c2Div3 * (2.0 * c2Div3 * c2Div3 - c1));
	    double q = mbDiv2 * mbDiv2 + aDiv3 * aDiv3 * aDiv3;
	    if (q > 0.0)
		q = 0.0;

	    double magnitude = sqrt(-aDiv3);
	    double angle = atan2(sqrt(-1.0 * q), mbDiv2) / 3.0;
	    double sn, cs;	//=sin(angle);double cs=cos(angle);
#ifdef __APPLE__
	__sincos(angle, &sn, &cs);
#else
	sincos(angle, &sn, &cs);
#endif
	    double magnitudecs = magnitude * cs;
	    double magnituderoot = magnitude * root3 * sn;

	    root0 = c2Div3 + 2.0 * magnitudecs;
	    root1 = c2Div3 - magnitudecs - magnituderoot;
	    root2 = c2Div3 - magnitudecs + magnituderoot;

	    if (root0 < 1e-6)
		root0 = 0;
	    if (root1 < 1e-6)
		root1 = 0;
	    if (root2 < 1e-6)
		root2 = 0;
	    double minr, maxr, midr;
	    minr = min(root0, min(root1, root2));
	    maxr = max(root0, max(root1, root2));
	    midr =
		(maxr == root0) ? (minr ==
				   root2 ? root1 : root2) : (minr ==
							     root2 ? root1
							     : root0);

	    double dwa =
		2.0 * (sqrt(maxr) + sqrt(midr) + sign * sqrt(minr));
	    float rms = 0.0;

	    rms = sqrt(Rg - dwa);
	    if (rms < 0.03)
		rms = 0.0f;

	    long index1d =
		(long) (iter + idx * (conformers - 1) -
			idx * (idx - 1) / 2);
	    result[index1d] = (unsigned short) (1000 * rms);
	    iter++;

	}			// end of j loop
    }				//end idx loop
    return result;
}
