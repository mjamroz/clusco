// arr - one dimensional array of coordinates, conformers - number of conformers, protlen - protein length. return one dimensional array (size N(N-1/2). 
//To get rmsd values - divide results array by 1000
unsigned short *rmsdCPU(float *arr, long conformers, int protlen);
unsigned short *rmsdCPUOneReference(float *reference, float *arr,
				    long conformers, int protlen);
