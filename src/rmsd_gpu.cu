#include <stdio.h>
#include <cuda.h>
#include "rmsd_gpu.h"
texture < float, cudaTextureType1D, cudaReadModeElementType > data_texture;
#include "rmsd_kernel.cu"

//#define CUDA_CHECK_ERROR



void checkCudaError()
{
//#ifdef CUDA_CHECK_ERROR
    cudaError e = cudaGetLastError();
    if (e != cudaSuccess) {
	fprintf(stderr,
		"Cuda ERROR: %s. if timeout, try to run without X-window\n",
		cudaGetErrorString(e));
	exit(-1);
    }
//#endif  // CUDA_CHECK_ERROR
}

unsigned short *calc_rmsd(float *apt, long conformers, int protlen)
{

    long arraylen = protlen * conformers * 3;
    long triangleLen = conformers * (conformers - 1) / 2;

    unsigned short *results = NULL;
    results = new unsigned short[triangleLen];

    float *d_apt;
    unsigned short *d_results;

    cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc < float >();


    cudaMalloc((void **) &d_results, sizeof(unsigned short) * triangleLen);
    checkCudaError();
    cudaMalloc((void **) &d_apt, sizeof(float) * arraylen);
    checkCudaError();

    //copy data into graphic card
    cudaMemcpy(d_apt, apt, sizeof(float) * arraylen,
	       cudaMemcpyHostToDevice);
    checkCudaError();

    cudaDeviceSynchronize();

    //define coordinates as texture memory
    cudaBindTexture(NULL, &data_texture, d_apt, &channelDesc,
		    sizeof(float) * arraylen);
    checkCudaError();

    // run computations
    rmsd <<< 65535, 16 >>> (d_apt, d_results, conformers, protlen);
    checkCudaError();

    // fetch results
    cudaMemcpy(results, d_results, sizeof(unsigned short) * triangleLen,
	       cudaMemcpyDeviceToHost);
    checkCudaError();
    // free memory
    cudaFree(d_results);
    cudaFree(d_apt);
    cudaUnbindTexture(data_texture);
    checkCudaError();
    return results;
}

unsigned short *calc_rmsdOneRef(float *reference, float *apt,
				long conformers, int protlen)
{

    long arraylen = protlen * conformers * 3 + protlen * 3;
    float *data = NULL;
    data = new float[arraylen];
    for (int i = 0; i < protlen * 3; i++)
	data[i] = reference[i];
    for (int i = protlen * 3; i < arraylen; i++)
	data[i] = apt[i - protlen * 3];

    unsigned short *results = NULL;
    results = new unsigned short[conformers];

    float *d_apt;
    unsigned short *d_results;

    cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc < float >();


    cudaMalloc((void **) &d_results, sizeof(unsigned short) * conformers);
    checkCudaError();
    cudaMalloc((void **) &d_apt, sizeof(float) * arraylen);
    checkCudaError();

    //copy data into graphic card
    cudaMemcpy(d_apt, data, sizeof(float) * arraylen,
	       cudaMemcpyHostToDevice);
    checkCudaError();

    cudaDeviceSynchronize();

    //define coordinates as texture memory
    cudaBindTexture(NULL, &data_texture, d_apt, &channelDesc,
		    sizeof(float) * arraylen);
    checkCudaError();

    // run computations
    rmsdOneReference <<< 65535, 16 >>> (d_apt, d_results, conformers,
					protlen);
    checkCudaError();

    // fetch results
    cudaMemcpy(results, d_results, sizeof(unsigned short) * conformers,
	       cudaMemcpyDeviceToHost);
    checkCudaError();
    // free memory
    cudaFree(d_results);
    cudaFree(d_apt);
    cudaUnbindTexture(data_texture);
    checkCudaError();
    delete[]data;
    return results;


}
