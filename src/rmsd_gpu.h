
unsigned short *calc_rmsd(float *apt, long conformers, int protlen);
unsigned short *calc_rmsdOneRef(float *reference, float *apt,
				long conformers, int protlen);
void checkCudaError();
