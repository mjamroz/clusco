
__global__ void rmsd(float *arr, unsigned short *result, long conformers,
		     int protlen)
{
    int idx = threadIdx.x + blockIdx.x * blockDim.x;

    if (conformers > idx + 1) {

	long iter = 0;
	for (int j = idx + 1; j < conformers; j++) {

	    int s_i = idx * protlen * 3;
	    int s_j = 3 * protlen * j;	//+3*protlen*conformers;

	    float covmat0, covmat1, covmat2, covmat3, covmat4, covmat5,
		covmat6, covmat7, covmat8, Rg;

	    covmat0 = covmat1 = covmat2 = covmat3 = covmat4 = covmat5 =
		covmat6 = covmat7 = covmat8 = Rg = 0.0f;
	    for (int a = 0; a < protlen * 3 - 2; a += 3) {

		float s_i_x = tex1Dfetch(data_texture, s_i + a);
		float s_i_y = tex1Dfetch(data_texture, s_i + a + 1);
		float s_i_z = tex1Dfetch(data_texture, s_i + a + 2);
		float s_j_x = tex1Dfetch(data_texture, s_j + a);
		float s_j_y = tex1Dfetch(data_texture, s_j + a + 1);
		float s_j_z = tex1Dfetch(data_texture, s_j + a + 2);

		covmat0 += s_i_x * s_j_x;
		covmat1 += s_i_y * s_j_x;
		covmat2 += s_i_z * s_j_x;
		covmat3 += s_i_x * s_j_y;
		covmat4 += s_i_y * s_j_y;
		covmat5 += s_i_z * s_j_y;
		covmat6 += s_i_x * s_j_z;
		covmat7 += s_i_y * s_j_z;
		covmat8 += s_i_z * s_j_z;
		Rg += s_i_x * s_i_x + s_j_x * s_j_x;
		Rg += s_i_y * s_i_y + s_j_y * s_j_y;
		Rg += s_i_z * s_i_z + s_j_z * s_j_z;

	    }

	    const double invlen = 1.0 / protlen;
	    Rg *= invlen;
	    covmat0 *= invlen;
	    covmat1 *= invlen;
	    covmat2 *= invlen;
	    covmat3 *= invlen;
	    covmat4 *= invlen;
	    covmat5 *= invlen;
	    covmat6 *= invlen;
	    covmat7 *= invlen;
	    covmat8 *= invlen;


	    // compute covariance matrix determinant 

	    float determinant =
		(covmat0 * (covmat4 * covmat8 - covmat5 * covmat7) -
		 covmat1 * (covmat3 * covmat8 - covmat5 * covmat6) +
		 covmat2 * (covmat3 * covmat7 - covmat4 * covmat6));
	    // check det sign
	    float sign = 1.0;
	    if (determinant < 0.0)
		sign = -1.0;

	    // square of covariance matrix, R2   
	    double r0 =
		covmat0 * covmat0 + covmat3 * covmat3 + covmat6 * covmat6;
	    double r1 =
		covmat0 * covmat1 + covmat3 * covmat4 + covmat6 * covmat7;
	    double r2 =
		covmat0 * covmat2 + covmat3 * covmat5 + covmat6 * covmat8;
	    double r4 =
		covmat1 * covmat1 + covmat4 * covmat4 + covmat7 * covmat7;
	    double r5 =
		covmat1 * covmat2 + covmat4 * covmat5 + covmat7 * covmat8;
	    double r8 =
		covmat2 * covmat2 + covmat5 * covmat5 + covmat8 * covmat8;



	    // calc eigenvalues 
	    double root0, root1, root2;
	    const double inv3 = 0.33333333333333333333333;
	    const double root3 = 1.732050807568877294;
	    double c0 =
		r0 * r4 * r8 + r1 * r2 * r5 + r1 * r2 * r5 - r0 * r5 * r5 -
		r4 * r2 * r2 - r8 * r1 * r1;
	    double c1 =
		r0 * r4 - r1 * r1 + r0 * r8 - r2 * r2 + r4 * r8 - r5 * r5;
	    double c2 = r0 + r4 + r8;
	    double c2Div3 = c2 * inv3;
	    double aDiv3 = (c1 - c2 * c2Div3) * inv3;
	    if (aDiv3 > 0.0)
		aDiv3 = 0.0;
	    double mbDiv2 =
		0.5 * (c0 +
		       c2Div3 * (c2Div3 * c2Div3 + c2Div3 * c2Div3 - c1));
	    double q = mbDiv2 * mbDiv2 + aDiv3 * aDiv3 * aDiv3;

	    if (q > 0.0)
		q = 0.0;

	    double angle = atan2(sqrt(-1.0 * q), mbDiv2) * inv3;
	    double magnitude = sqrt(-aDiv3);
	    double sn, cs;	//=sin(angle);double cs=cos(angle);
	    sincos(angle, &sn, &cs);
	    double magnitudecs = magnitude * cs;
	    double magnituderoot = magnitude * root3 * sn;

	    root0 = c2Div3 + magnitudecs + magnitudecs;
	    root1 = c2Div3 - magnitudecs - magnituderoot;
	    root2 = c2Div3 - magnitudecs + magnituderoot;

	    if (root0 < 1e-6)
		root0 = 0.0;
	    if (root1 < 1e-6)
		root1 = 0.0;
	    if (root2 < 1e-6)
		root2 = 0.0;


	    double minr, maxr, midr;
	    minr = min(root0, min(root1, root2));
	    maxr = max(root0, max(root1, root2));
	    midr =
		(maxr == root0) ? (minr ==
				   root2 ? root1 : root2) : (minr ==
							     root2 ? root1
							     : root0);


	    double dwa = 2 * (sqrt(maxr) + sqrt(midr) + sign * sqrt(minr));

	    float rms = sqrt(Rg - dwa);
	    if (rms < 0.001)
		rms = 0.0f;

	    long index1d =
		iter + idx * (conformers - 1) - idx * (idx - 1) / 2;
	    result[index1d] = (unsigned short) (1000 * rms);

	    iter++;

	}			// end of j loop
    }

}

__global__ void rmsdOneReference(float *arr, unsigned short *result,
				 long conformers, int protlen)
{
    int idx = threadIdx.x + blockIdx.x * blockDim.x;

    if (conformers >= idx && idx > 0) {


	int s_i = idx * protlen * 3;
	float covmat0, covmat1, covmat2, covmat3, covmat4, covmat5,
	    covmat6, covmat7, covmat8, Rg;

	covmat0 = covmat1 = covmat2 = covmat3 = covmat4 = covmat5 =
	    covmat6 = covmat7 = covmat8 = Rg = 0.0f;
	for (int a = 0; a < protlen * 3 - 2; a += 3) {

	    float s_i_x = tex1Dfetch(data_texture, s_i + a);
	    float s_i_y = tex1Dfetch(data_texture, s_i + a + 1);
	    float s_i_z = tex1Dfetch(data_texture, s_i + a + 2);
	    float s_j_x = tex1Dfetch(data_texture, a);
	    float s_j_y = tex1Dfetch(data_texture, a + 1);
	    float s_j_z = tex1Dfetch(data_texture, a + 2);

	    covmat0 += s_i_x * s_j_x;
	    covmat1 += s_i_y * s_j_x;
	    covmat2 += s_i_z * s_j_x;
	    covmat3 += s_i_x * s_j_y;
	    covmat4 += s_i_y * s_j_y;
	    covmat5 += s_i_z * s_j_y;
	    covmat6 += s_i_x * s_j_z;
	    covmat7 += s_i_y * s_j_z;
	    covmat8 += s_i_z * s_j_z;
	    Rg += s_i_x * s_i_x + s_j_x * s_j_x;
	    Rg += s_i_y * s_i_y + s_j_y * s_j_y;
	    Rg += s_i_z * s_i_z + s_j_z * s_j_z;

	}

	const double invlen = 1.0 / protlen;
	Rg *= invlen;
	covmat0 *= invlen;
	covmat1 *= invlen;
	covmat2 *= invlen;
	covmat3 *= invlen;
	covmat4 *= invlen;
	covmat5 *= invlen;
	covmat6 *= invlen;
	covmat7 *= invlen;
	covmat8 *= invlen;


	// compute covariance matrix determinant 

	float determinant =
	    (covmat0 * (covmat4 * covmat8 - covmat5 * covmat7) -
	     covmat1 * (covmat3 * covmat8 - covmat5 * covmat6) +
	     covmat2 * (covmat3 * covmat7 - covmat4 * covmat6));
	// check det sign
	float sign = 1.0;
	if (determinant < 0.0)
	    sign = -1.0;

	// square of covariance matrix, R2   
	double r0 =
	    covmat0 * covmat0 + covmat3 * covmat3 + covmat6 * covmat6;
	double r1 =
	    covmat0 * covmat1 + covmat3 * covmat4 + covmat6 * covmat7;
	double r2 =
	    covmat0 * covmat2 + covmat3 * covmat5 + covmat6 * covmat8;
	double r4 =
	    covmat1 * covmat1 + covmat4 * covmat4 + covmat7 * covmat7;
	double r5 =
	    covmat1 * covmat2 + covmat4 * covmat5 + covmat7 * covmat8;
	double r8 =
	    covmat2 * covmat2 + covmat5 * covmat5 + covmat8 * covmat8;



	// calc eigenvalues 
	double root0, root1, root2;
	const double inv3 = 0.33333333333333333333333;
	const double root3 = 1.732050807568877294;
	double c0 =
	    r0 * r4 * r8 + r1 * r2 * r5 + r1 * r2 * r5 - r0 * r5 * r5 -
	    r4 * r2 * r2 - r8 * r1 * r1;
	double c1 =
	    r0 * r4 - r1 * r1 + r0 * r8 - r2 * r2 + r4 * r8 - r5 * r5;
	double c2 = r0 + r4 + r8;
	double c2Div3 = c2 * inv3;
	double aDiv3 = (c1 - c2 * c2Div3) * inv3;
	if (aDiv3 > 0.0)
	    aDiv3 = 0.0;
	double mbDiv2 =
	    0.5 * (c0 + c2Div3 * (c2Div3 * c2Div3 + c2Div3 * c2Div3 - c1));
	double q = mbDiv2 * mbDiv2 + aDiv3 * aDiv3 * aDiv3;

	if (q > 0.0)
	    q = 0.0;

	double angle = atan2(sqrt(-1.0 * q), mbDiv2) * inv3;
	double magnitude = sqrt(-aDiv3);
	double sn, cs;		//=sin(angle);double cs=cos(angle);
	sincos(angle, &sn, &cs);
	double magnitudecs = magnitude * cs;
	double magnituderoot = magnitude * root3 * sn;

	root0 = c2Div3 + magnitudecs + magnitudecs;
	root1 = c2Div3 - magnitudecs - magnituderoot;
	root2 = c2Div3 - magnitudecs + magnituderoot;

	if (root0 < 1e-6)
	    root0 = 0.0;
	if (root1 < 1e-6)
	    root1 = 0.0;
	if (root2 < 1e-6)
	    root2 = 0.0;


	double minr, maxr, midr;
	minr = min(root0, min(root1, root2));
	maxr = max(root0, max(root1, root2));
	midr =
	    (maxr == root0) ? (minr == root2 ? root1 : root2) : (minr ==
								 root2 ?
								 root1 :
								 root0);


	double dwa = (sqrt(maxr) + sqrt(midr) + sign * sqrt(minr));

	double rms = sqrt(Rg - dwa - dwa);
	if (rms < 1e-3)
	    rms = 0.0;

	result[idx - 1] = (unsigned short) (1000.0 * rms);



    }

}
