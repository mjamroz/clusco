#include <fstream>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <vector>
#ifndef NOOMP
#include <omp.h>
#endif
using namespace std;
#include "utils.h"

void saveResults2D(char *filename, float **results, long ndim)
{
    stringstream output;


    for (long row = 0; row < ndim; row++) {
	output << setw(10) << row;
	for (int col = 0; col < ndim; col++) {
	    float v = results[row][col];
	    if (v < 0.02)
		v = 0.0;
	    output << setw(7) << setprecision(3) << v;
	    //dim2[row][row+iter] = dim2[row+iter][row] = dim1[j]/1000.0;
	}
	output << endl;

    }

    ofstream myfile;
    myfile.open(filename, ios::out);
    myfile.setf(ios::showpoint);
    myfile << output.str();
    myfile.close();

}

int *getMedoids(float **scores, int *clusters, int k, int conformers)
{
    float **average;
    average = new float *[k];

    int *medoid = new int[k];
    float *mdist = new float[k];

    for (int i = 0; i < k; i++) {
	medoid[i] = -1;
	mdist[i] = 1e32;
    }
    //  return medoid;


    for (int i = 0; i < k; i++) {
	average[i] = new float[conformers];
	for (int j = 0; j < conformers; j++)
	    average[i][j] = 0.0;

    }
    for (int i = 0; i < conformers; i++) {
	for (int j = 0; j < conformers; j++) {
	    average[clusters[i]][j] += scores[i][j];
    }}

    for (int i = 0; i < k; i++)
	for (int j = 0; j < conformers; j++)
	    average[i][j] /= conformers;



    for (int j = 0; j < conformers; j++) {
	int cluster = clusters[j];
	float distance = 0.0;
	for (int i = 0; i < conformers; i++) {
	    float d = average[cluster][i] - scores[j][i];
	    distance += d * d;
	}
	if (distance < mdist[cluster]) {
	    //    cout << cluster << " " <<distance<<" "<<j<<endl;
	    mdist[cluster] = distance;
	    medoid[cluster] = j;
	}
    }

    return medoid;
}

float *avgScore(float **scores, int *clusters, int k, int conformers)
{
    float *avg = new float[k];
    for (int i = 0; i < k; i++) {
	avg[i] = 0.0;
	vector < int >c;

	for (int j = 0; j < conformers; j++)	//which models belongs to cluster i
	    if (clusters[j] == i)
		c.push_back(j);
	unsigned int size = c.size();
	if (size > 1) {
	    for (unsigned int j = 0; j < size - 1; j++) {
		for (unsigned int k = j + 1; k < size; k++) {
		    avg[i] += (scores[c[j]][c[k]]);
		}
	    }
	    avg[i] /= (size * (size - 1) / 2);
	} else if (size == 1)
	    avg[i] = scores[0][0];	//diagonal element is score for the same model

	//cout << i << " " << avg[i] <<endl;
    }


    return avg;
}

void saveClusteringResults(char *in_filename, int *clusters,
			   bool iftrajectory, int k, int conformers,
			   char *header, float **scores)
{
    int *medoids = getMedoids(scores, clusters, k, conformers);
    float *avgscore = avgScore(scores, clusters, k, conformers);
    vector < string > filenames;
    stringstream doutput, doutput2;
    string output = string(in_filename) + ".clustering0";


    doutput << header << endl;
    if (!iftrajectory) {	//read protein filenames from in_filename list

	string line;

	ifstream lists;
	lists.open(in_filename, ifstream::in);
	while (getline(lists, line)) {
	    if (!line.empty())
		filenames.push_back(line);
	}
	lists.close();

    } else {			// name proteins like in trajectory/multimodel pdb
	//(ostringstream::out);

	for (int i = 0; i < conformers; i++) {
	    ostringstream ss;
	    ss << i;
	    filenames.push_back(ss.str());
	}
    }
    int *counts = new int[k];
    for (int i = 0; i < k; i++)
	counts[i] = 0;
#pragma omp parallel for
    for (int i = 0; i < k; i++) {
	for (int j = 0; j < conformers; j++) {
	    if (clusters[j] == i)
		counts[i]++;

	}
    }

// sort clusters
    int *c = new int[k];
    for (int i = 0; i < k; i++)
	c[i] = i;
    for (int i = 0; i < k - 1; i++)
	for (int j = i + 1; j < k; j++) {
	    if (counts[i] < counts[j]) {
		int tmpidx = c[i];
		int tmpcounts = counts[i];
		counts[i] = counts[j];
		c[i] = c[j];
		c[j] = tmpidx;
		counts[j] = tmpcounts;
	    }
	}
//for (int i=0;i<k;i++)
//cout << c[i] << " " <<counts[i] <<endl;

    // not the fastest
    for (int i = 0; i < k; i++) {
	if (counts[i] == 0)
	    continue;

	doutput << setw(4) << i << " : " << setw(30) <<
	    filenames[medoids[c[i]]] << " : " << setw(7) << setprecision(3)
	    << ((float) counts[i] /
		conformers) << " : " << setprecision(2) << avgscore[c[i]]
	    << " : ";
	for (int j = 0; j < conformers; j++) {
	    if (clusters[j] == c[i]) {
		doutput << filenames[j] << " ";
	    }
	}
	doutput << endl;;
    }

    ofstream writefile;
    writefile.open(output.c_str(), ios::out);
    writefile << doutput.str();
    writefile.close();

    // simple writing clusters
    output = string(in_filename) + ".clustering1";

    doutput2 << header << endl;
    for (int j = 0; j < conformers; j++) {
	doutput2 << setw(30) << filenames[j] << setw(5) << clusters[j];
	if (medoids[clusters[j]] == j)
	    doutput2 << " *";
	doutput2 << endl;

    }
    writefile.open(output.c_str(), ios::out);
    writefile << doutput2.str();
    writefile.close();
}

void saveResults(char *filename, short unsigned int *results, long ndim,
		 bool one)
{
    stringstream output;	// = new stringstream();
    ofstream myfile;
    myfile.open(filename, ios::out);
    myfile.setf(ios::showpoint);
    long globiter = 0;
    if (!one) {
	for (long row = 0; row < ndim; row++) {
	    long iter = 1;
	    long start = row * (ndim - 1) - row * (row - 1) / 2;
	    long stop = (row + 1) * (ndim - 1) - (row + 1) * (row) / 2;
	    for (long j = start; j < stop; j++) {
		float v = results[j] / 1000.0;
		if (v < 0.02)
		    v = 0.0;
		output << setw(10) << row << setw(10) << row +
		    iter << setw(7) << setprecision(3) << v << endl;
		globiter++;
		//dim2[row][row+iter] = dim2[row+iter][row] = dim1[j]/1000.0;
		if (globiter == 1000000) {
		    myfile << output.str();
		    output.str("");
		    globiter = 0;
		}
		iter++;
	    }

	}
    } else {
	for (long row = 0; row < ndim; row++) {
	    float v = results[row] / 1000.0;
	    if (v < 0.02)
		v = 0.0;

	    output << setw(10) << 0 << setw(10) << row << setw(7) <<
		setprecision(3) << v << endl;
	}
    }

    myfile << output.str();
    myfile.close();

}

void reshapeArray(unsigned short *dim1, float **dim2, long ndim, bool rmsd)
{
    long i, iter;
    for (i = 0; i < ndim; i++) {
	iter = 1;
	long start = i * (ndim - 1) - i * (i - 1) / 2;
	long stop = (i + 1) * (ndim - 1) - (i + 1) * (i + 1 - 1) / 2;
	for (long j = start; j < stop; j++) {

	    dim2[i][i + iter] = dim2[i + iter][i] = dim1[j] / 1000.0;
	    iter++;
	}
    }

//for(int z=0;z<ndim;z++)
    // for(int n=0;n<ndim;n++)
    //if (dim2[n][z]>30 || dim2[n][z]<0.5)
//                      if(n!=z)
//                              printf("%d %d %6.2f\n",n,z,dim2[n][z]);


    if (rmsd) {
	for (i = 0; i < ndim; i++)
	    dim2[i][i] = 0.0;
    } else {
	for (i = 0; i < ndim; i++)
	    dim2[i][i] = 1.0;
    }


}
