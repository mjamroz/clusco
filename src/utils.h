void reshapeArray(unsigned short *dim1, float **dim2, long ndim,
		  bool rmsd);
void saveResults(char *filename, short unsigned int *results, long ndim,
		 bool one);
void saveResults2D(char *filename, float **results, long ndim);
void saveClusteringResults(char *in_filename, int *clusters,
			   bool iftrajectory, int k, int conformers,
			   char *header, float **scores);
float *avgScore(float **scores, int *clusters, int k, int conformers);
